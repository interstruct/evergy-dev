<?php

/**
 * Upload CSV file from $_FILES
 * 
 * @return string|bool Returns file path or false if were some problems.
 */
function uploadCSV() {
    $uploaddir = QBPI__PLUGIN_DIR . 'admin/csv/';
    $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
        return $uploadfile;
    } else {
        return false;
    }
}

/**
 * Sanitize importing data depends of column
 * 
 * @param string $data Data from imported column
 * @param string $column Column number
 * @uses correctEncoding() to correct encoding
 * @used-by updateProjectMeta()
 * @return string Returns sanitized data
 */
function sanitizeData($data, $column) {

    $dataCorr = correctEncoding($data);

    switch ($column) {
        case "8":
            $dataCorr = str_replace(";", "; ", $dataCorr);
            //echo"replaced: ".$dataCorr." Column: ".$column;
            break;
        case "15":
            $dataCorr = preg_replace('/^\h*\v+/m', '', $dataCorr);
            break;
        case "17":
            $dataCorr = preg_replace('/^\h*\v+/m', '', $dataCorr);
            break;
        case "19":
            $dataCorr = preg_replace('/^\h*\v+/m', '', $dataCorr);
            break;
        default:
            break;
    }
    return $dataCorr;
}

/**
 * Correct encoding of string
 * 
 * @param string $data Data from imported column
 * @param string $column Column number
 * @used-by sanitizeData()
 * @return string Returns data with corrected encoding
 */
function correctEncoding($data) {
    $encFrom = "ASCII";
    $encTo = "UTF-8";
    //echo mb_detect_encoding($data);
    $dataCorr = mb_convert_encoding($data, $encTo, $encFrom);
    return $dataCorr;
}

/**
 * Check if the post with project_id is in DB
 * 
 * @param string $project_id ID of project in DB
 * @used-by createProject()
 * @return string|bool Returns ID if the project is in DB or false if not
 */
function isUpdatable($project_id) {

    $args = array(
        'meta_query' => array(
            array(
                'key' => 'column_1',
                'value' => $project_id,
                'compare' => '=',
            )
        ),
        'post_type' => 'projects'
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        $posts = $query->posts;
        foreach ($posts as $post) {
            return $post->ID;
        }
    } else {
        return false;
    }
}

/**
 * Update or create project meta
 * 
 * @param array $dataArr Data from imported row
 * @param string $post_id post (project) ID
 * @uses sanitizeData() to sanitize data
 * @uses update_post_meta() to update post meta
 * @used-by createProject()
 * @return bool Returns true if all meta were created/updated and false if not
 */
function updateProjectMeta($dataArr, $post_id) {
    $noErrors = true;
    for ($i = 0; $i < 25; $i++) {
        $result = update_post_meta($post_id, 'column_' . ($i + 1), sanitizeData($dataArr[$i], ($i + 1)));
        if (!$result) {
            $noErrors = $result;
        }
    }
    $status = 'brief';
    if ($dataArr[9] == 'Y') {
        $status = 'case';
    }

    wp_set_post_terms($post_id, $status, 'project_status', true);

    return $noErrors;
}

/**
 * Create project post from prepared data
 * 
 * @param array $dataArr Data from imported row
 * @uses isUpdatable() to check if project already is in DB
 * @uses correctEncoding() to correct encoding of some strings
 * @uses wp_insert_post() to create post (project)
 * @used-by importFromCsv()
 * @return nothind
 */
function createProject($dataArr) {

    $msg = "imported";
    $msg1 = "were imported with errors";
    $partlyResultClass = "import-partly-error";

    if (!isUpdatable($dataArr[0])) {
        $post_data = array(
            'post_title' => wp_strip_all_tags(correctEncoding($dataArr[3])),
            'post_status' => 'publish',
            'post_author' => 1,
            'post_type' => 'projects',
        );
        $post_id = wp_insert_post($post_data);
    } else {
        $post_id = isUpdatable($dataArr[0]);
        $msg = "updated";
        $msg1 = "have not been updated (the values are same)";
        $partlyResultClass = "import-partly-success";
    }

    if ($post_id == 0) {
        echo "<p class='import-error'>(ID: " . $dataArr[0] . ") There was a problem creating the project '" . correctEncoding($dataArr[3]) . "'. Project was not imported.</p>";
        return;
    }

    if (!updateProjectMeta($dataArr, $post_id)) {
        echo "<p class='" . $partlyResultClass . "'>(ID: " . $dataArr[0] . ") Project '" . correctEncoding($dataArr[3]) . "' was " . $msg . ", but some fields " . $msg1 . "</p>";
        return;
    }
    echo "<p class='import-success'>(ID: " . $dataArr[0] . ") Project '" . correctEncoding($dataArr[3]) . "' was " . $msg . "!</p>";
}

function validateCsv($filename = '', $delimiter = ',', $startRow, $endRow) {


    if (!file_exists($filename)) {
        echo "<p class='import-error'>The file was not uploaded to the server.<p>";
        return false;
    }

    if (!is_readable($filename)) {
        echo "<p class='import-error'>The file does not readable. Check permissions for this file please.<p>";
        return false;
    }


    $rowCount = 0;

    if (($handle = fopen($filename, 'r')) !== FALSE) {
        while (($row = fgetcsv($handle, 5000, $delimiter)) !== FALSE) {
            $rowCount++;

            if ($rowCount > $endRow) {
                break;
            }

            //check number of columns
            if ($rowCount == 1 || $rowCount == 2) {
                if (count($row) != 25) {
                    echo "<p class='import-error'>Invalid file structure. The table should consist of 25 columns. First row should consist of numbers of columns. Second row should consist of titles for the columns.<p>";
                    return false;
                }
            }


            if ($rowCount > ($startRow - 1)) {
                //check cells formats

                if ($row[0] == '') {
                    echo "<p class='import-error'>Cell 'Project ID' can't be empty. (Row: " . $rowCount . ")<p>";
                    return false;
                }
                if (!is_numeric($row[0])) {
                    echo "<p class='import-error'>Required format for cell 'Project ID': Numeric. (Row: " . $rowCount . ")<p>";
                    return false;
                }


                if ($row[1] != '' && !is_numeric($row[1])) {
                    echo "<p class='import-error'>Required format for cell 'Latitude': Numeric. (Row: " . $rowCount . ")<p>";
                    return false;
                }


                if ($row[2] != '' && !is_numeric($row[2])) {
                    echo "<p class='import-error'>Required format for cell 'Longitude': Numeric. (Row: " . $rowCount . ")<p>";
                    return false;
                }


                if ($row[3] == '') {
                    echo "<p class='import-error'>Cell 'Project Name' can't be empty. (Row: " . $rowCount . ")<p>";
                    return false;
                }


                if ($row[4] == '') {
                    echo "<p class='import-error'>Cell 'Country' can't be empty. (Row: " . $rowCount . ")<p>";
                    return false;
                }


                if ($row[5] != 'PV' && $row[5] != 'Wind') {
                    echo "<p class='import-error'>Invalid Type of Project. It must be 'PV' or 'Wind'. (Row: " . $rowCount . ")<p>";
                    return false;
                }


                if ($row[6] == '') {
                    echo "<p class='import-error'>Cell 'Year of Mandate' can't be empty. (Row: " . $rowCount . ")<p>";
                    return false;
                }
                if ($row[6] < 2000 || $row[6] > 2100) {
                    echo "<p class='import-error'>Cell 'Year of Mandate' should have year format. The valid range is 2000-2100. (Row: " . $rowCount . ")<p>";
                    return false;
                }


                if ($row[7] == '') {
                    echo "<p class='import-error'>Cell 'Evergy Scope of Work' can't be empty. (Row: " . $rowCount . ")<p>";
                    return false;
                }


                if ($row[8] == '') {
                    echo "<p class='import-error'>Cell 'Installed Capacity in MW' can't be empty. (Row: " . $rowCount . ")<p>";
                    return false;
                }
                if (!is_numeric($row[8])) {
                    echo "<p class='import-error'>Required format for cell 'Installed Capacity in MW': Numeric. (Row: " . $rowCount . ")<p>";
                    return false;
                }


                if ($row[9] != 'Y' && $row[9] != 'N') {
                    echo "<p class='import-error'>Invalid Homepage Case. It must be 'Y' or 'N'.(Row: " . $rowCount . ")<p>";
                    return false;
                }

                if ($row[9] == 'Y') {

                    if ($row[10] == '') {
                        echo "<p class='import-error'>Cell 'Client' can't be empty. (Row: " . $rowCount . ")<p>";
                        return false;
                    }


                    if ($row[11] == '') {
                        echo "<p class='import-error'>Cell 'Year of Commissioning' can't be empty. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                    if ($row[11] < 2000 || $row[11] > 2100) {
                        echo "<p class='import-error'>Cell 'Year of Commissioning' should have year format. The valid range is 2000-2100. (Row: " . $rowCount . ")<p>";
                        return false;
                    }


                    if ($row[12] == '') {
                        echo "<p class='import-error'>Cell 'Descriptive Headline' can't be empty. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                    if ($row[13] == '') {
                        echo "<p class='import-error'>Cell 'Copytext' can't be empty. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                    if ($row[14] == '') {
                        echo "<p class='import-error'>Cell 'Key Technical Data' can't be empty. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                    if ($row[15] == '') {
                        echo "<p class='import-error'>Cell 'Headlie 1' can't be empty. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                    if ($row[16] == '') {
                        echo "<p class='import-error'>Cell 'Bullets 1' can't be empty. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                    if ($row[17] == '') {
                        echo "<p class='import-error'>Cell 'Headlie 2' can't be empty. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                    if ($row[18] == '') {
                        echo "<p class='import-error'>Cell 'Bullets 2' can't be empty. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                }
                if ($row[24] != '') {
                    if (!is_numeric($row[24])) {
                        echo "<p class='import-error'>Required format for cell 'N° of Turbines': Integer. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                    if ((((double)$row[24])-floor((double)$row[24]))!=0){
                        echo "<p class='import-error'>Required format for cell 'N° of Turbines': Integer. (Row: " . $rowCount . ")<p>";
                        return false;
                    }
                    
                }
            }
        }
        $rowCount = 0;
        fclose($handle);
    }
    return true;
}

/**
 * Import data from CSV file to DB
 * 
 * @param string $filename path to csv file
 * @param string $delimeter delimeter for CSV file
 * @param string $startRow number of row from which to start importing
 * @param string $endRow number of row with which to stop importing
 * @uses file_exists() to check if the file is
 * @uses is_readable() to check permissions to file
 * @uses fopen() to open file stream
 * @uses fgetcsv() to parse csv file
 * @return nothind
 */
function importFromCsv($filename = '', $delimiter = ',', $startRow, $endRow) {

    if (validateCsv($filename, $delimiter, $startRow, $endRow)) {
        echo "<p class='import-success'>Import was completed successfully</p>";
    } else {
        echo "<p class='import-error'>Import was aborted by validator.</p>";
    }
    /*
      if (file_exists($filename) && is_readable($filename)) {
      $rowCount = 0;

      if (($handle = fopen($filename, 'r')) !== FALSE) {
      while (($row = fgetcsv($handle, 5000, $delimiter)) !== FALSE) {
      $rowCount++;

      if ($rowCount > $endRow) {
      break;
      }

      if ($rowCount > ($startRow - 1)) {
      if ($row[0] != "") {
      createProject($row);
      }
      }
      }
      $rowCount = 0;
      fclose($handle);
      }
      } */
}
?>

