document.addEventListener('DOMContentLoaded', function () {
    if (document.querySelector(".import-form")) {
        document.querySelector(".import-form").addEventListener('submit', function () {
            document.querySelector(".import-form-button").value = "Please wait...";
            document.querySelector(".import-form-button").disabled = true;
        });
    }
});
