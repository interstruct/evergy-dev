<div class='wrap'>
    <h1>QB Projects Import</h1>

    <hr/>
    <form action="" method="POST" class="import-form" enctype="multipart/form-data">
        <label class="qbpi-file-label"><h4>Choose CSV file to import</h4></label>
        <input type="file" name="userfile" accept=".csv" required>
        <label class="qbpi-file-label"><h4>Start from (row number):</h4></label>
        <input type="number" name="startrow" step="1" value="3" min="1" required>
        <label class="qbpi-file-label"><h4>Finish with (row number):</h4></label>
        <input type="number" name="endrow" step="1" value="100" required min="1">
        <br><br><input type="submit" class="import-form-button" value="Import">
    </form>
    <hr/>
<?php
    include QBPI__PLUGIN_DIR . 'admin/includes/import_core.php';

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $importFile = uploadCSV();
        echo "<h4>Import Results:<h4>Import from: " . $importFile . ". <br>Import Rows: " . $_POST['startrow'] . " - " . $_POST['endrow'];
        if (!$importFile) {
            echo "<p class='import-error'>There was some problem CSV file uploading. Projects were not imported.</p>";
        } else {
            importFromCsv($importFile, ';', $_POST['startrow'], $_POST['endrow']);
        }
    }
    ?>

</div>
