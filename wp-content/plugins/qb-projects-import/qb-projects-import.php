<?php

/*
  Plugin Name:  QB Projects Import
  Description:  Using this plugin you can adapt images and backgrounds on your site.
  Version:      1.0.0
  Author:       QBeeS Solutions
  Author URI:   http://qbees.pro
  License:      GPL2
  License URI:  https://www.gnu.org/licenses/gpl-2.0.html
  Text Domain:  qbai
  Domain Path:  /languages

  QB Projects Import is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  any later version.

  QB Projects Import is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with QB Projects Import. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('QBPI__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('QBPI__PLUGIN_URI', plugin_dir_url(__FILE__));

/**
 * Enqueue admin scripts and styles
 */
function qbpi_admin_enqueues() {
    wp_enqueue_style('qbpi-css', QBPI__PLUGIN_URI . '/admin/css/admin-styles.css');
    wp_enqueue_script('qbpi-js', QBPI__PLUGIN_URI . '/admin/js/admin-script.js', array('jquery'), '', true);
}

add_action('admin_enqueue_scripts', 'qbpi_admin_enqueues');

/**
 * Setup admin dashboard menu item for the plugin 
 */
function qbpi_setup_menu() {
    add_menu_page('Projects Import', 'Projects Import', 'manage_options', 'qbpi-settings', 'qbpi_admin_page', QBAI__PLUGIN_URI . '/admin/img/qbees_logo.png');
}

/**
 * Setup admin dashboard page for the plugin 
 */
function qbpi_admin_page() {
    require_once QBPI__PLUGIN_DIR . '/admin/index.php';
}

add_action('admin_menu', 'qbpi_setup_menu');