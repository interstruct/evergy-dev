<?php 
/**
* dkpdf-footer.php
* This template is used to display content in PDF Footer
*
* Do not edit this template directly, 
* copy this template and paste in your theme inside a directory named dkpdf 
*/ 
?>

<?php 
	global $post;
        
        
        if (get_post_type() == 'projects') {
            $pdf_footer_show_pagination = false;
        } else {
            $pdf_footer_show_pagination = true;
        }
?>



<?php
	// only enter here if any of the settings exists
	if($pdf_footer_show_pagination) { ?>

	    <div style="width:100%;float:left;padding-top:10px;">
		    <div style="float:left;text-align:left; padding-left: 15px; font-family: hindmadurailight">

				<?php
					// check if Footer show pagination is checked
					if ( $pdf_footer_show_pagination ) {

						echo apply_filters( 'dkpdf_footer_pagination', '{PAGENO}' );

					} 

				?>

		    </div>
	    </div>

	<?php }

?>



