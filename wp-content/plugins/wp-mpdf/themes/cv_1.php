<?php
//Standard Plan Template

global $post;
global $pdf_output;
global $pdf_header;
global $pdf_footer;

global $pdf_template_pdfpage;
global $pdf_template_pdfpage_page;
global $pdf_template_pdfdoc;

global $pdf_html_header;
global $pdf_html_footer;


//Set a pdf template. if both are set the pdfdoc is used. (You didn't need a pdf template)
$pdf_template_pdfpage 		= ''; //The filename off the pdf file (you need this for a page template)
$pdf_template_pdfpage_page 	= 1;  //The page off this page (you need this for a page template)

$pdf_template_pdfdoc  		= ''; //The filename off the complete pdf document (you need only this for a document template)

$pdf_html_header 			= false; //If this is ture you can write instead of the array a html string on the var $pdf_header
$pdf_html_footer 			= false; //If this is ture you can write instead of the array a html string on the var $pdf_footer



while (have_posts()) : the_post();

    $photo = wp_get_attachment_image(get_field('photo')['ID'], array('600', '600'), "", array("style" => "width: 600px; height: 600px; display: block;margin-top: 30px;border-radius: 100px;"));
    $name = get_field('first_name') . " " . substr(get_field('last_name'), 0, 1);
    $position = get_field('title');
    $summary = get_field('summary');
    $languages = '';
    $education = '';

    if (have_rows('skills')):
        while (have_rows('skills')) : the_row();
            $skills .= '<tr class="title-table grey" align="center">
                    <td style="height:50px;vertical-align: middle;font-size: 19px;text-align:center;">' . get_sub_field('type') . '</td>
                    <td style="height:50px;vertical-align: middle;font-size: 19px;text-align:center;">' . get_sub_field('name') . '</td>
                    <td style="height:50px;vertical-align: middle;font-size: 19px;text-align:center;">' . get_sub_field('level') . '</td>
                    <td style="height:50px;vertical-align: middle;font-size: 19px;text-align:center;">' . get_sub_field('last_used') . '</td>
                    <td style="height:50px;vertical-align: middle;font-size: 19px;text-align:center;">' . get_sub_field('years') . '</td>
                </tr>';
            
        endwhile;
    endif;


    if (have_rows('projects')):
        while (have_rows('projects')) : the_row();

            if (!empty(get_sub_field('link'))) {
                $link = '<tr class="links-project">
                                        <td>
                                            <p>&nbsp;</p>
                                            <p style="font-size: 20px;line-height: 22px;">Link:</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="' . get_sub_field('link') . '" style="color: #231f20;font-size: 23px;">' . get_sub_field('link') . '</a>
                                        </td>
                                    </tr>';
            } else { $link = ''; }

            
            if (get_sub_field('link')!=""){
                $link = '<tr class="links-project">
                            <td>
                                    <p>&nbsp;</p>
                                <p style="font-size: 20px;line-height: 22px;">Link:</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="'. get_sub_field('link') .'" style="color: #231f20;font-size: 23px;">'. get_sub_field('link') .'</a>
                                    <p>&nbsp;</p>
                            </td>
                        </tr>';
            } else { $link = ''; }
            
            $projects .= '<tr style="border-bottom: 1px solid #bcbec0;">
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td class="title-project" style="font-weight: bold;font-size: 25px;margin-bottom: 10px;">' . get_sub_field('title') . '</td>
                            </tr>
                            <tr>
                                <td class="title-project" style="font-weight: bold;font-size: 25px;margin-bottom: 10px;">' . get_sub_field('role') . '</td>
                            </tr>
                            <tr style="height: 50px;">
                                <td style="vertical-align: middle;"><p style="width:500px;font-size: 20px;">' . get_sub_field('description') . '</p></td>
                            </tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
                            '. $link .'
                        </table>
                    </td>
                </tr>';

        endwhile;
    endif;
    
    
    
    
    if( have_rows('languages') ):
        
    $languages .= '<tr>
        <td align="left">
            <h4 style="font-weight: bold;font-size: 28px;margin-bottom: 30px;text-transform: uppercase;text-align: left;">languages</h4>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="left">
                <p class="subtitle" style="font-weight: bold;font-size: 26px;">Spoken Level</p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr align="left">
        <td align="left">
            <table align="left">';
        
        while( have_rows('languages') ) : the_row();
            
            
            $languages .= '<tr align="left" style="text-align:left;">
                    <td style="vertical-align: middle;font-size: 24px;padding: 5px 15px;border-bottom: 10px solid #fec340;">'.get_sub_field('language').'</td>
                    <td style="font-size: 24px;padding: 5px 15px;border-bottom: 10px solid #fec340;font-weight: bold;background: #231f20;color: #fec240;">'.get_sub_field('written_level').'</td>
            </tr>
            <tr>
                    <td>&nbsp;</td>
            </tr>';
            
 
        endwhile;
        
        $languages .= '</table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="left">
                <p class="subtitle" style="font-weight: bold;font-size: 26px;">Written Level</p>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="left">
                <table>';
        
        while( have_rows('languages') ) : the_row();

            $languages .= '<tr>
                <td style="vertical-align: middle;font-size: 24px;padding: 5px 15px;border-bottom: 10px solid #fec340;">'.get_sub_field('language').'</td>
                <td style="font-size: 24px;padding: 5px 15px;border-bottom: 10px solid #fec340;font-weight: bold;background: #231f20;color: #fec240;">'.get_sub_field('spoken_level').'</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>';
                
        endwhile;
        
        $languages .= '</table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>';        

    endif;
    
    if( have_rows('education') ):
        
    $education .= '<tr>
        <td align="left">
            <h4 style="text-transform: uppercase;font-weight: bold;font-size: 28px;margin-bottom: 30px;">education</h4>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="left">
            <table width="500px">';    
        
    while( have_rows('education') ) : the_row();
               
        $education .= '<tr>
            <td>
                <p style="font-weight: bold;font-size: 20px;">'.get_sub_field('start_year').' - '.get_sub_field('end_year').'</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 20px;padding: 10px;line-height: 24px;margin-bottom: 20px;">'.get_sub_field('school').'. '.get_sub_field('degree').'</p>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>';
    endwhile;
    
    $education .= '</table>
        </td>
    </tr>';
    
    endif; 
                

endwhile;



$pdf_output_body = '<table style="width:100%;width: 1170px;margin: 0 auto;table-layout:fixed;">
    <tr>
        <td style="vertical-align: top;border-right: 15px solid #fff;border-left: 15px solid #fff;">
            <table>
                <tr>
                    <td>
                        <div class="logo">
                            <img src="http://cv.qbees.pro/wp-content/themes/qbeescv/img/logo.png" alt="" style="width: 282px;">
                        </div>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td class="main-content">
                        <h1 style="font-weight: bold;font-size: 39px;margin: 50px 0 20px;text-transform: uppercase;">profile summary</h1>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <p style="font-size: 25px;line-height: 28px;padding-right: 40px;">'.$summary.'</p>
                    </td>
                </tr>
            </table>
            <table class="skills-table">
                <thead align="center">
                    <tr style="background: #fec240;">
                        <th style="height:50px;vertical-align: middle;font-size: 20px;">Category</th>
                        <th style="height:50px;vertical-align: middle;font-size: 20px;">Competence</th>
                        <th style="height:50px;vertical-align: middle;font-size: 20px;">Level</th>
                        <th style="height:50px;vertical-align: middle;font-size: 20px;">Last used</th>
                        <th style="height:50px;vertical-align: middle;font-size: 20px;text-align:center;">Years</th>
                    </tr>
                </thead>
                '.$skills.'
            	<tr>
            		<td>&nbsp;</td>
            	</tr>
            	<tr>
            		<td>&nbsp;</td>
            	</tr>
            </table>
            <table>
            	<tr>
            		<td>&nbsp;</td>
            	</tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
            	<tr>
            		<td><h2 class="projects-title text-uppercase">selected projects</h2></td>
            	</tr>
            </table>
            <table width="100%" class="projects-block">
            '.$projects.'
            </table>
        </td>
        <td class="sidebar" style="padding: 0 50px;" align="center">
        <table>
        	<tr>
				<td>
					<!--<img src="http://cv.qbees.pro/wp-content/uploads/2017/10/avatar.jpg" alt="" style="width: 231px;display: block;margin: 60px auto 30px;border-radius: 100px;">-->
                                        '.$photo.'
				</td>
			</tr>
                        <tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<h2 style="font-size: 46px;padiing-top:60px;margin-top:60px;font-weight: bold;text-align: center;line-height: 15px;">'.$name.'</h2>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<h3 style="text-align: center;font-weight: bold;font-size: 24px;">'.$position.'</h3>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			'.$languages.'
			'.$education.'
		</table>
        </td>
    </tr>
</table>';



$pdf_output = '<!DOCTYPE html><html><head>'.$pdf_output_head.'</head><body>'.$pdf_output_body.'</body></html>';
?>
