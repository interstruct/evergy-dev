<?php
$styles = `a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;vertical-align:baseline}fieldset,img{margin:0;padding:0;vertical-align:baseline}html{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;box-sizing:border-box}a img,fieldset,img{border:none}input[type=email],input[type=tel],input[type=text],textarea{-webkit-appearance:none}button,input[type=submit]{cursor:pointer}button::-moz-focus-inner,input[type=submit]::-moz-focus-inner{padding:0;border:0}textarea{overflow:auto}button,input{margin:0;padding:0;border:0}a,a:focus,button,div,h1,h2,h3,h4,h5,h6,input,select,span,textarea{outline:none}ol,ul{list-style-type:none}table{border-spacing:0;border-collapse:collapse;width:100%}*,:after,:before{box-sizing:inherit}@font-face{font-family:"hindmadurailight";src:url(../fonts/hindmadurai-light-webfont.woff2) format("woff2"),url(../fonts/hindmadurai-light-webfont.woff) format("woff");font-weight:400;font-style:normal}@font-face{font-family:"hindmaduraisemibold";src:url(../fonts/hindmadurai-semibold-webfont.woff2) format("woff2"),url(../fonts/hindmadurai-semibold-webfont.woff) format("woff");font-weight:400;font-style:normal}.p-case__list li,.p-text__list li,body{font-family:'hindmadurailight',Arial,Helvetica,sans-serif;font-weight:400}body{background:#fff;color:#000;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.out{width:790px;margin:0 auto;padding:0 16px;overflow:hidden}.p-header{width:100%;padding-left:17%;padding-top:32px;padding-bottom:24px}.p-header__logo{width:160px;margin-left:-83px}.p-header__logo img{width:100%;display:block}.p-case__head{position:relative;width:100%;padding:54px 8.5% 128px 17%;background-repeat:no-repeat;background-size:cover;background-position:50% 50%;margin-bottom:43px}.p-case__title{width:443px;font-size:42px;line-height:1.22;font-family:'hindmaduraisemibold',Arial,Helvetica,sans-serif;font-weight:400;text-decoration:underline}.p-case__aside{width:23%;padding-left:17%;float:left;box-sizing:content-box}.p-case__content{width:49.5%;padding-right:8.5%;float:right;box-sizing:content-box}.p-case__list{padding-right:22px}.p-case__list li,.p-text__list li{font-size:11px;line-height:1.25;margin-bottom:11px}.p-case__list strong,.p-case__subtitle{font-family:'hindmaduraisemibold',Arial,Helvetica,sans-serif;font-weight:400}.p-case__list strong{display:block;line-height:1.75}.p-case__list p{margin-bottom:3px}.p-case__subtitle{font-size:24px;line-height:1.13;margin-bottom:36px}.p-case__text{margin-bottom:17px}.p-text__title{font-size:15px;line-height:1.18;font-family:'hindmaduraisemibold',Arial,Helvetica,sans-serif;font-weight:400;margin-bottom:13px}.p-text__list{list-style-type:disc;padding-left:20px}.p-text__list li{font-size:13px;line-height:1.2;margin-bottom:4px}.clearfix{clear:both}.p-footer{padding-top:52px}.p-footer__copy,.p-footer__link{box-sizing:content-box;font-weight:400;font-size:11px;line-height:1.25}.p-footer__link{padding-left:17%;width:41%;float:left;font-family:'hindmaduraisemibold',Arial,Helvetica,sans-serif}.p-footer__link a{color:#000;text-decoration:none}.p-footer__copy{width:33.5%;padding-right:8.5%;float:right;text-align:right;font-family:'hindmadurailight',Arial,Helvetica,sans-serif}@media (prefers-reduced-motion:reduce){*{transition:none!important}`;
$pdf_output = '<!doctype html>
<html>

  <head>
    <meta charset="utf-8" />
    <title>Case PDF</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#fff" />
    <meta name="format-detection" content="telephone=no" />
    <link href="https://fonts.googleapis.com/css?family=Hind+Madurai:300,400,500,600,700&amp;subset=latin-ext,tamil" rel="stylesheet" />
    <link rel="stylesheet" media="all" href="css/case-pdf.css" />
'.$styles.'    
  </head>
  <body>
    <div class="out">
      <div class="p-header">
        <div class="p-header__logo">
          <img src="img/brand_evergy.svg" alt="" />
        </div>
      </div>
      <div class="p-case">
        <div class="p-case__head" style="background-image: url(img/header.jpg);">
          <h1 class="p-case__title">PV Power Plant Gamma, Romania</h1>
        </div>
        <div class="p-case__in">
          <div class="p-case__aside">
            <ul class="p-case__list">
              <li>
                <strong>Country</strong>
                <p>France</p>
              </li>
              <li>
                <strong>Client</strong>
                <p>Allianz Capital Partners</p>
              </li>
              <li>
                <strong>Year of Mandate </strong>
                <p>2010</p>
              </li>
              <li>
                <strong>Year of Commisioning </strong>
                <p>2019</p>
              </li>
              <li>
                <strong>Key Technical Data</strong>
                <p>Total Capacity 50 MWp</p>
                <p>PV Technology Poly Crystalline</p>
                <p>Inverter Manufacturer SMA Solar Technology</p>
                <p>Grid Connection on 110 kV HV public grid</p>
                <p>8 MVA Reactive Power Compensation</p>
              </li>
            </ul>
          </div>
          <div class="p-case__content">
            <h2 class="p-case__subtitle">Technical Due Diligence and Technical Controlling of a 50 MWp PV Power Plant in Romania</h2>
            <div class="p-case__text p-text">
              <h3 class="p-text__title">The objectives of the assignment were:</h3>
              <ul class="p-text__list">
                <li>Identification of technical risks and development of measures for limitation and mitigation</li>
                <li>Management of Technical Acceptance including Acceptance Testing</li>
                <li>Technical Controlling</li>
              </ul>
            </div>
            <div class="p-case__text p-text">
              <h3 class="p-text__title">The scope of work covered the following aspects:</h3>
              <ul class="p-text__list">
                <li>Determination of Expected Annual Energy Yield</li>
                <li>Licenses and Permits</li>
                <li>Technical Design Review</li>
                <li>Grid Connection</li>
                <li>Support in Negotiation of Project Contracts</li>
                <li>Construction Monitoring</li>
                <li>Verification of production numbers</li>
                <li>Regular plant inspections (yearly)</li>
                <li>Consultancy in repair and budget questions</li>
                <li>Assistance in warranty and insurance claims</li>
                <li>Support in operational and economical improvements</li>
                <li>Regular reporting to Owners / Banks</li>
              </ul>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <footer class="p-footer">
        <div class="p-footer__link">more cases @ <a href="">www.evergy.de/cases</a></div>
        <div class="p-footer__copy">Evergy Engineering GmbH ©2019</div>
        <div class="clearfix"></div>
      </footer>
    </div>
  </body>
</html>';

?>
