jQuery(document).ready(function ($) {
    var resizableImages = $(".qbai-image"),
            imagesData = [],
            resizableBackground = $(".qbai-background"),
            windowResolution = $(window).width();

    // Resizable images
    resizableImages.each(function (position, item) {
        var curItem = $(item);
        imagesData.push(curItem.data());
    });

    $.ajax({
        type: "POST",
        url: qbai_ajax.url,
        data: {
            action: "qbai",
            images: imagesData,
            resolution: windowResolution
        },
        success: function (data) {

            data = JSON.parse(data);

            resizableImages.each(function (position, item) {
                var curItem = $(item),
                        responseImageItem = data.find(function (responseItem) {
                            return curItem.data("image-id") == responseItem.imageId && curItem.data("image-type") == responseItem.imageType
                        }),
                        isAppliebleForSrcChange = responseImageItem && responseImageItem.src;

                if (!isAppliebleForSrcChange) {
                    return;
                }

                var isBackgroundImage = curItem.hasClass("qbai-background");


                if (isBackgroundImage) {
                    curItem.css('background-image', 'url(' + responseImageItem.src + ')');
                    return;
                } else {
                    curItem.attr("src", responseImageItem.src);
                }

            });
        }
    });

});