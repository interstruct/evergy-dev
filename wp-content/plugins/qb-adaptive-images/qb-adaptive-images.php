<?php

/*
  Plugin Name:  QB Adaptive Images
  Description:  Using this plugin you can adapt images and backgrounds on your site.
  Version:      1.0.0
  Author:       QBeeS Solutions
  Author URI:   http://qbees.pro
  License:      GPL2
  License URI:  https://www.gnu.org/licenses/gpl-2.0.html
  Text Domain:  qbai
  Domain Path:  /languages

  QB Adaptive Images is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  any later version.

  QB Adaptive Images is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with QB Adaptive Images. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('QBAI__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('QBAI__PLUGIN_URI', plugin_dir_url(__FILE__));


/**
 * Setup admin dashboard menu item for the plugin 
 */
function qbai_setup_menu() {
    add_menu_page('Adaptive Images by QBeeS', 'Adaptive Images', 'manage_options', 'qbai-settings', 'qbai_admin_page', QBAI__PLUGIN_URI . '/admin/img/qbees_logo.png');
}


/**
 * Setup admin dashboard page for the plugin 
 */
function qbai_admin_page() {
    require_once QBAI__PLUGIN_DIR . '/admin/index.php';
}
add_action('admin_menu', 'qbai_setup_menu');


/**
 * Register qbai_rules post type.
 */
function qbai_register_rules() {

    $labels = array(
        "name" => __("QBAI Rules", "qb"),
        "singular_name" => __("QBAI Rules", "qb"),
    );

    $args = array(
        "label" => __("QBAI Rules", "qb"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "qbai_rules", "with_front" => true),
        "query_var" => true,
        "supports" => array("title"),
    );
    register_post_type("qbai_rules", $args);
}
add_action('init', 'qbai_register_rules');


/**
 * Get registered adaptivity rules
 * 
 * @return array Returns array with all adaptivity rules info
 */
function get_qbai_sizes() {

    $rules = array();

    $posts = get_posts(array(
        'numberposts' => -1,
        'category' => 0,
        'orderby' => 'title',
        'order' => 'ASC',
        'include' => array(),
        'exclude' => array(),
        'meta_key' => '',
        'meta_value' => '',
        'post_type' => 'qbai_rules',
        'suppress_filters' => true,
    ));

    foreach ($posts as $post) {
        setup_postdata($post);
        $options = get_post_meta($post->ID, '', true);
        $rule_ID = get_the_title($post->ID);
        $rules[$rule_ID] = array();
        foreach ($options AS $key => $option) {
            $rules[$rule_ID][$key] = json_decode($option[0]);
        }
        krsort($rules[$rule_ID]);
    }
    return $rules;
}


// Register image sizes
$image_sizes = get_qbai_sizes();

if (function_exists('add_image_size')) {
    foreach ($image_sizes as $image_rules => $image_sizes_arr) {
        foreach ($image_sizes_arr as $screen_size => $image_size) {
            add_image_size('qb-' . $image_rules . '-' . $screen_size, $image_size[0], $image_size[1], true);
        }
    }
}


/**
 * Enqueue public scripts
 */
function qbai_public_enqueues() {
    wp_enqueue_script('qb-adaptive-images', QBAI__PLUGIN_URI . '/public/js/main.js', '', '', true);

    wp_localize_script('qb-adaptive-images', 'qbai_ajax', array(
        'url' => admin_url('admin-ajax.php')
            )
    );
}
add_action('wp_enqueue_scripts', 'qbai_public_enqueues');


/**
 * Enqueue admin scripts and styles
 */
function qbai_admin_enqueues() {
    wp_enqueue_style('qbai-css', QBAI__PLUGIN_URI . '/admin/css/admin-styles.css');
    wp_enqueue_script('qbai-js', QBAI__PLUGIN_URI . '/admin/js/admin-script.js', array('jquery'), '', true);
}
add_action('admin_enqueue_scripts', 'qbai_admin_enqueues');


/**
 * Callback for admin main page. It returns to AJAX json with resized images data.
 */
function qbai_callback() {
    $cb_images = [];
    $in_images = $_POST['images'];
    $curr_res = $_POST['resolution'];

    foreach ($in_images AS $in_image) {
        $right_image_size = qbai_get_image_size($curr_res, $in_image['imageType']);
        $image_src = wp_get_attachment_image_src($in_image['imageId'], 'qb-' . $in_image['imageType'] . '-' . $right_image_size);
        $cb_images[] = array(
            'imageId' => $in_image['imageId'],
            'src' => $image_src[0],
            'size' => 'qb-' . $in_image['imageType'] . '-' . $right_image_size,
            'imageType' => $in_image['imageType']
        );
    }

    echo(json_encode($cb_images));

    die();
    wp_die();
}
add_action('wp_ajax_qbai', 'qbai_callback');
add_action('wp_ajax_nopriv_qbai', 'qbai_callback');


/**
 * Decide which image size is better for current screen size
 * 
 * @param int $screen_res Current screen width
 * @param string $image_rules Name of rules list
 * @return int Returns best image width for current screen size
 */
function qbai_get_image_size($screen_res, $image_rules) {

    $image_sizes = get_qbai_sizes();

    foreach ($image_sizes[$image_rules] as $screen_size => $image_size) {
        if ($screen_res >= $image_size[0]) {
            return $screen_size;
        }
    }
}


/**
 * Returns ready IMG tag or block element injection
 * 
 * @param int $image_id - ID of attachment
 * @param string $image_rules - Name of rules list
 * @param bool $bg - if element background-image injection needed
 * @param array $classes - Additional classes
 * @param array $data_atts - Additional data attributes
 * @return string - $bg === true will return block tag injection, $bg === false will return ready image tag
 */
function qbai_get_image($image_id, $image_rules, $bg = false, $classes = array(), $data_atts = array()) {

    $cb_classes = "";
    $cb_data_atts = "";

    foreach ($classes as $class) {
        $cb_classes .= ' ' . $class;
    }

    foreach ($data_atts as $key => $data_attr) {
        $cb_data_atts .= ' ' . $key . ' = "' . $data_attr . '"';
    }

    if ($bg) {
        return ' class="qbai-image qbai-background' . $cb_classes . '" data-image-type="' . $image_rules . '" data-image-id="' . $image_id . '" ' . $cb_data_atts;
    }
    return '<img src="" class="qbai-image' . $cb_classes . '" data-image-type="' . $image_rules . '" data-image-id="' . $image_id . '" >' . $cb_data_atts;
}
add_action('wp_ajax_qbai_add_action', 'qbai_add_action_callback');



function qbai_add_action_callback() {

    $options = $_POST['options'];
    $img_type = $_POST['img_type'];
    $rule_ID = $_POST['rule_ID'];

    if (!$rule_ID) {

        $post_id = wp_insert_post(wp_slash(array(
            'post_status' => 'publish',
            'post_type' => 'qbai_rules',
            'post_title' => $img_type,
            'post_author' => 1,
            'ping_status' => get_option('default_ping_status'),
            'post_parent' => 0,
            'menu_order' => 0,
            'to_ping' => '',
            'pinged' => '',
            'post_password' => '',
            'post_excerpt' => '',
        )));

        if ($post_id > 0 && count($options) > 0) {
            foreach ($options AS $key => $option) {
                add_post_meta($post_id, $key, json_encode($option));
            }
        }

        if ($post_id > 0) {
            $response = array(
                "mess" => "Rule (ID: " . $post_id . ") successfully created.",
                "action" => "add",
                "rule-id" => $post_id,
                "status" => 1
            );
        } else {
            $response = array(
                "mess" => "Error while creating the rule.",
                "action" => "add",
                "status" => 0
            );
        }
    } else {

        $upd_meta_status = 1;

        foreach ($options AS $key => $option) {
            $upd_meta_result = update_post_meta($rule_ID, $key, json_encode($option));
        }

        $rule_attr = array(
            'ID' => $rule_ID,
            'post_title' => $img_type,
        );


        $upd_title_status = wp_update_post($rule_attr);

        if ($upd_title_status > 0 && $upd_meta_status > 0) {
            $response = array(
                "mess" => "Rule (ID: " . $rule_ID . ") successfully updated.",
                "action" => "update",
                "status" => 1
            );
        } else {
            $response = array(
                "mess" => "Error while updating the rule.",
                "action" => "update",
                "status" => 0
            );
        }
    }
    echo json_encode($response);
    wp_die();
}
add_action('wp_ajax_qbai_remove_action', 'qbai_remove_action_callback');



function qbai_remove_action_callback() {

    $rule_ID = $_POST['rule_ID'];
    $options = get_post_meta($rule_ID, '', true);

    foreach ($options AS $key) {
        delete_post_meta($rule_ID, $key);
    }

    $remove_result = wp_delete_post($rule_ID);

    if (is_object($remove_result)) {
        $response = array(
            "mess" => "Rule (ID: " . $post_id . ") successfully deleted.",
            "action" => "remove",
            "status" => 1
        );
    } else {
        $response = array(
            "mess" => "Error while deleting the rule.",
            "action" => "remove",
            "status" => 0
        );
    }
    echo json_encode($response);
    wp_die();
}
add_action('wp_ajax_qbai_remove_option_action', 'qbai_remove_option_action_callback');



function qbai_remove_option_action_callback() {

    $rule_ID = $_POST['rule_ID'];
    $option_ID = $_POST['option_ID'];
    $remove_result = delete_post_meta($rule_ID, $option_ID);

    if ($remove_result == true) {
        $response = array(
            "mess" => "Option (Name: " . $rule_ID . ") successfully deleted.",
            "action" => "remove_option",
            "status" => 1
        );
    } else {
        $response = array(
            "mess" => "Error while deleting the option.",
            "action" => "remove_option",
            "status" => 0
        );
    }
    echo json_encode($response);
    wp_die();
}
