<div class='wrap'>
    <h1>QB Adaptive Images</h1>

    <ul class="admin-tabs">
        <li class="tab-link current" data-tab="tab-1">Rules</li>
        <li class="tab-link" data-tab="tab-2">FAQ</li>
    </ul>

    <div id="tab-1" class="tab-content current">
        <button class="add-rule-btn">Add rule</button>
        <ul class="rule-block">
            <?php
            $posts = get_posts(array(
                'numberposts' => -1,
                'category' => 0,
                'orderby' => 'date',
                'order' => 'ASC',
                'include' => array(),
                'exclude' => array(),
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'qbai_rules',
                'suppress_filters' => true,
            ));

            foreach ($posts as $post) {
                setup_postdata($post);
                ?>
                <li class="block-with-rules" rule-id="<?php echo $post->ID; ?>">
                    <input placeholder="Please enter the image type" value="<?php echo get_the_title($post); ?>" type="text" name="image-type" class="image-type-rule">
                    <button class="add-rule-option-btn">Add option</button>
                    <button class="save-rule-btn">Update</button>
                    <button class="remove-rule-btn">Remove</button>
                    <form class="rule-form" method="post" action="submit.php">
                        <?php
                        $options = get_post_meta($post->ID, '', true);
                        foreach ($options AS $key => $option) {
                            if (is_int($key)) {
                                $option = json_decode($option[0]);
                                ?>
                                <div class="rule-form-row">
                                    <input placeholder="Screen width" value="<?php echo $key; ?>" type="number" name="screen" min="1">
                                    <input placeholder="Image Width" value="<?php echo $option[0] ?>" type="number" name="width" min="1">
                                    <input placeholder="Image Height" value="<?php echo $option[1] ?>" type="number" name="height" min="1">
                                    <button class="delete-rule-btn" type="button">X</button>
                                </div>
                                <?php
                            }
                        }
                        ?>


                    </form>
                    <div class="rule-message"></div>
                </li>
                <?php
            }

            wp_reset_postdata();
            ?>
        </ul>
    </div>
    <div id="tab-2" class="tab-content">
        <h2>Is some function that I should to use for QBAI?</h2>
        <code>qbai_get_image($image_id, $image_rule, $bg, $classes, $data_atts) // Use this function for images adaptation.</code>  
        <p>
            <b>$image_id | required</b> <i>(int)</i> - Image ID.
        </p>
        <p>
            <b>$image_rule | required</b> <i>(string)</i> - Name of adaptivity rule.
        </p>
        <p>
            <b>$bg</b> <i>(bool)</i> - TRUE - using function for block background adaptation. FALSE (default) - using function for IMG tag.
        </p>
        <p>
            <b>$classes</b> <i>(array)</i> - array of additional classes. (Do not use inline classes if you already use this function)
        </p>
        <p>
            <b>$data_atts</b> <i>(array)</i> - array of additional data attributes.
        </p>
        
        <h3>Examples:</h3>
        <i>USE FOR IMAGE:</i><br>
        <code>qbai_get_image(123, 'staff-photo', FALSE, array('staff-photo-class'), array('test-attr' => 'OK'))</code>
        <p>
        <i>RESULT:</i> <br><code>&lt;img src="http://dev4.qbees.tech/wp-content/uploads/2018/11/example-staff-90x160.jpg" class="qbai-image staff-photo-class" test-attr="OK" data-image-type="staff-photo" data-image-id="123"&gt;</code>
        </p>
        <i>USE FOR BACKGROUND:</i><br>
        <code>&lt;div &lt;?php echo qbai_get_image(123, 'staff-photo', TRUE, array('staff-photo-class'), array('test-attr' => 'OK')); ?&gt; &gt;&lt;/div&gt;</code>
        <p>
        <i>RESULT:</i> <br><code>&lt;div class="qbai-image qbai-background staff-photo-class" style="background-image: url('http://dev4.qbees.tech/wp-content/uploads/2018/11/example-staff-90x160.jpg')" test-attr="OK" data-image-type="staff-photo" data-image-id="123"&gt; &lt;/div&gt;</code>
    </div>
</div>