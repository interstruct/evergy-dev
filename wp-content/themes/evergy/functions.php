<?php

// disable admin bar on frontend
show_admin_bar(false);

// Disable unnecessary includes
add_filter('xmlrpc_enabled', '__return_false');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_oembed_add_host_js');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('wp_head', 'wp_shortlink_wp_head');

// Disable customizer options

function evergy_remove_customizer_options($wp_customize) {
    $wp_customize->remove_section('title_tagline');
    $wp_customize->remove_section('editor');
    $wp_customize->remove_section('nav');
    //$wp_customize->remove_section('themes');
}

add_action('customize_register', 'evergy_remove_customizer_options', 30);

//remove emoji totaly
remove_action('wp_head', 'print_emoji_detection_script', 7); // Front-end browser support detection script
remove_action('admin_print_scripts', 'print_emoji_detection_script'); // Admin browser support detection script
remove_action('wp_print_styles', 'print_emoji_styles'); // Emoji styles
remove_action('admin_print_styles', 'print_emoji_styles'); // Admin emoji styles
remove_filter('the_content_feed', 'wp_staticize_emoji'); // Remove from feed, this is bad behaviour!
remove_filter('comment_text_rss', 'wp_staticize_emoji'); // Remove from feed, this is bad behaviour!
remove_filter('wp_mail', 'wp_staticize_emoji_for_email'); // Remove from mail
add_filter('emoji_svg_url', '__return_false'); // Remove DNS prefetch s.w.org (used for emojis, since WP 4.7)
// register menu location.
register_nav_menus(array(
    'primary' => __('Primary Menu', 'evergy'),
    'footer' => __('Footer Menu', 'evergy'),
));

// Enable acf options page
if (function_exists('acf_add_options_page')) {
    $parent = acf_add_options_page(array(
        'page_title' => 'Theme Options',
        'menu_title' => 'Theme Options',
        'menu_slug' => 'evergy-theme-options',
        'capability' => 'edit_posts',
        'parent_slug' => '',
        'position' => false,
        'icon_url' => 'dashicons-schedule',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'General Theme Options',
        'menu_title' => 'General',
        'parent_slug' => $parent['menu_slug'],
    ));
}

// Notices
function evergy_admin_notice() {
    if (!function_exists('acf')) {
        echo '<div class="notice notice-error">
             <p>Evergy Theme: Plugin ACF pro is required.</p>
         </div>';
    }
}

//get builder enqueues
function evergy_get_builder_enqueues() {
    while (have_posts()) {
        the_post();
        if (have_rows('page_builder')) {
            while (have_rows('page_builder')) {
                the_row();
                if (file_exists(get_template_directory() . '/template-parts/builder/' . get_row_layout() . '/' . get_row_layout() . '.css')) {
                    wp_enqueue_style('evergy-builder-' . get_row_layout(), get_stylesheet_directory_uri() . '/template-parts/builder/' . get_row_layout() . '/' . get_row_layout() . '.css');
                }
                if (file_exists(get_template_directory() . '/template-parts/builder/' . get_row_layout() . '/' . get_row_layout() . '.js')) {
                    wp_enqueue_script('evergy-builder-' . get_row_layout() . '-js', get_stylesheet_directory_uri() . '/template-parts/builder/' . get_row_layout() . '/' . get_row_layout() . '.js', '', '', true);
                }

                if (get_row_layout() == 'topbanner') {
                    wp_enqueue_style('evergy-builder-job-teaser', get_stylesheet_directory_uri() . '/template-parts/builder/' . get_row_layout() . '/job-teaser.css');
                    wp_enqueue_script('evergy-builder-job-teaser', get_stylesheet_directory_uri() . '/template-parts/builder/' . get_row_layout() . '/job-teaser.js', '', '', true);
                }
            }
        }
    }
}

// enqueues
function evergy_enqueues() {

    // enqueue styles
    //wp_enqueue_style('evergy-css-fonts', get_stylesheet_directory_uri() . '/assets/css/fonts.css');

    wp_enqueue_style('wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Hind+Madurai:300,400,500,600,700&amp;subset=latin-ext,tamil', false);
    wp_enqueue_style('evergy-css-common', get_stylesheet_directory_uri() . '/assets/css/common.css');
    wp_enqueue_style('evergy-css-header', get_stylesheet_directory_uri() . '/assets/css/header.css');
    wp_enqueue_style('evergy-css-footer', get_stylesheet_directory_uri() . '/assets/css/footer.css');

    wp_enqueue_script('evergy-js-common', get_stylesheet_directory_uri() . '/assets/js/common.js', '', '', true);
    wp_enqueue_script('evergy-js-touchswipe', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.min.js', '', '', true);



    //get builder enqueues
    evergy_get_builder_enqueues();

    wp_localize_script('evergy-js-common', 'myajax', array(
        'url' => admin_url('admin-ajax.php')
            )
    );

    wp_enqueue_script('evergy-example-ajax', get_stylesheet_directory_uri() . '/assets/js/example-ajax.js', '', '', true);

    if (is_page_template('templates/map.php')) {
        wp_enqueue_script('evergy-mapbox', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.js', '', '', true);
        wp_enqueue_script('evergy-map', get_stylesheet_directory_uri() . '/assets/js/map.js', '', '', true);
        wp_enqueue_style('evergy-map-css', get_stylesheet_directory_uri() . '/assets/css/map.css');
    }

    if (is_page_template('templates/map_blank.php')) {
        wp_enqueue_script('evergy-mapbox', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.js', '', '', true);
        wp_enqueue_script('evergy-map', get_stylesheet_directory_uri() . '/assets/js/map.js', '', '', true);
        wp_enqueue_style('evergy-map-css', get_stylesheet_directory_uri() . '/assets/css/map.css');
    }

    //get enqueues for specific post types tempates

    if ('projects' == get_post_type()) {
        wp_enqueue_style('evergy-case-css', get_stylesheet_directory_uri() . '/assets/css/case.css');
        wp_enqueue_style('evergy-quote-slider-css', get_stylesheet_directory_uri() . '/template-parts/builder/quote-slider-automatic/quote-slider-automatic.css');
        wp_enqueue_script('evergy-quote-slider-js', get_stylesheet_directory_uri() . '/template-parts/builder/quote-slider-automatic/quote-slider-automatic.js', '', '', true);
    }

    wp_enqueue_style('evergy-css-custom', get_stylesheet_directory_uri() . '/assets/css/custom.css');
}

add_action('wp_enqueue_scripts', 'evergy_enqueues');

function evergy_admin_enqueues() {
    wp_enqueue_style('evergy-admin-css', get_stylesheet_directory_uri() . '/assets/css/admin.css');
    wp_enqueue_script('evergy-mapjson-ajax', get_stylesheet_directory_uri() . '/assets/js/mapjson.js', '', '', true);
    wp_enqueue_script('evergy-pdf-gen-ajax', get_stylesheet_directory_uri() . '/assets/js/pdf-gen.js', '', '', true);
    wp_enqueue_script('evergy-admin', get_stylesheet_directory_uri() . '/assets/js/admin.js', '', '', true);
}

add_action('admin_enqueue_scripts', 'evergy_admin_enqueues');

// print component wrapper classes
function evergy_the_component_wrapper() {
    $stickyNavClass = '';
    $stickyNavId = '';
    $stickyNavTitle = '';
    $stylingClasses = '';
    if (get_sub_field('add_to_sticky_navigation')) {
        $stickyNavClass = 'js-sticky-scroll-block';
        $stickyNavId = ' data-id="' . sanitize_title(get_sub_field('sticky_navigation_item_title')) . '" ';

        if (!empty(get_sub_field('anchor'))) {
            $scrollOpNavId = ' data-other-page-scroll-id="' . get_sub_field('anchor') . '" ';
            $stickyNavId = ' data-id="' . get_sub_field('anchor') . '" ';
        } else {
            $scrollOpNavId = ' data-other-page-scroll-id="' . sanitize_title(get_sub_field('sticky_navigation_item_title')) . '" ';
            $stickyNavId = ' data-id="' . sanitize_title(get_sub_field('sticky_navigation_item_title')) . '" ';
        }
        $stickyNavTitle = ' data-name="' . get_sub_field('sticky_navigation_item_title') . '" ';
    }
    if (get_sub_field('background_color') == 'white') {
        $stylingClasses = 'is-white ';
    }
    echo 'class="' . sanitize_title(get_row_layout()) . ' ' . $stickyNavClass . ' ' . $stylingClasses . '"' . $stickyNavId . $stickyNavTitle . $scrollOpNavId;
}

/**
 * Responsive Image Helper Function
 *
 * @param string $image_id the id of the image (from ACF or similar)
 * @param string $image_size the size of the thumbnail image or custom image size
 * @param string $max_width the max width this image will be shown to build the sizes attribute 
 */
function evergy_responsive_image($image_id, $image_size, $max_width) {

    // check the image ID is not blank
    if ($image_id != '') {

        // set the default src image size
        $image_src = wp_get_attachment_image_url($image_id, $image_size);

        // set the srcset with various image sizes
        $image_srcset = wp_get_attachment_image_srcset($image_id, $image_size);

        // generate the markup for the responsive image
        echo 'src="' . $image_src . '" srcset="' . $image_srcset . '" sizes="(max-width: ' . $max_width . ') 100vw, ' . $max_width . '"';
    }
}

/* Customize admin listing for projects */

add_filter('manage_projects_posts_columns', 'qbpi_projects_table_head');

function qbpi_projects_table_head($defaults) {
    $defaults['project_id'] = 'ID';
    $defaults['project_country'] = 'Country';
    $defaults['project_client'] = 'Client';
    $defaults['project_category'] = 'Category';
    $defaults['project_type'] = 'Type';
    $defaults['project_capacity'] = 'Capacity (MW)';
    $defaults['project_pdf'] = 'PDF';

    unset($defaults['date']);

    return $defaults;
}

add_action('manage_projects_posts_custom_column', 'qbpi_projects_table_content', 10, 2);

function qbpi_projects_table_content($column_name, $post_id) {

    if ($column_name == 'project_id') {
        echo get_post_meta($post_id, 'column_1', true);
    }

    if ($column_name == 'project_country') {
        echo get_post_meta($post_id, 'column_5', true);
    }

    if ($column_name == 'project_client') {
        echo get_post_meta($post_id, 'column_11', true);
    }

    if ($column_name == 'project_category') {
        $cat = get_post_meta($post_id, 'column_10', true);
        if ($cat == "Y") {
            echo "Case";
        } else if ($cat == "N") {
            echo "Brief";
        }
    }

    if ($column_name == 'project_type') {
        echo get_post_meta($post_id, 'column_6', true);
    }

    if ($column_name == 'project_capacity') {
        echo get_post_meta($post_id, 'column_9', true);
    }

    if ($column_name == 'project_pdf') {
        $cat = get_post_meta($post_id, 'column_10', true);
        if ($cat == "Y") {
            echo "<a target='_blank' href='" . get_the_permalink($post_id) . "/?pdf=" . $post_id . "'>Download</a>";
        } else {
            echo "-";
        }
    }
}

add_filter('manage_edit-projects_sortable_columns', 'qbpi_projects_table_sorting');

function qbpi_projects_table_sorting($columns) {
    $columns['project_id'] = 'project_id';
    $columns['project_category'] = 'project_category';
    $columns['project_country'] = 'project_country';
    $columns['project_client'] = 'project_client';
    $columns['project_type'] = 'project_type';
    $columns['project_capacity'] = 'project_capacity';
    return $columns;
}

add_action('pre_get_posts', 'qbpi_category_orderby');

function qbpi_category_orderby($query) {
    if (!is_admin())
        return;

    $orderby = $query->get('orderby');

    if ('project_id' == $orderby) {
        $query->set('meta_key', 'column_1');
        $query->set('orderby', 'meta_value_num');
        $query->set('meta_type', 'numeric');
    }

    if ('project_category' == $orderby) {
        $query->set('meta_key', 'column_10');
        $query->set('orderby', 'meta_value');
        $query->set('meta_type', 'char');
    }

    if ('project_country' == $orderby) {
        $query->set('meta_key', 'column_5');
        $query->set('orderby', 'meta_value');
        $query->set('meta_type', 'char');
    }

    if ('project_client' == $orderby) {
        $query->set('meta_key', 'column_11');
        $query->set('orderby', 'meta_value');
        $query->set('meta_type', 'char');
    }

    if ('project_type' == $orderby) {
        $query->set('meta_key', 'column_6');
        $query->set('orderby', 'meta_value');
        $query->set('meta_type', 'char');
    }
    if ('project_capacity' == $orderby) {
        $query->set('meta_key', 'column_9');
        $query->set('orderby', 'meta_value_num');
        $query->set('meta_type', 'numeric');
    }
}

//quotes and news filtering

function qbpi_filter_quotes($post_type, $which) {

    // Apply this only on a specific post type
    if ('quotes_and_news' !== $post_type)
        return;

    // A list of taxonomy slugs to filter by
    $taxonomies = array('q_n_category');

    foreach ($taxonomies as $taxonomy_slug) {

        // Retrieve taxonomy data
        $taxonomy_obj = get_taxonomy($taxonomy_slug);
        $taxonomy_name = $taxonomy_obj->labels->name;

        // Retrieve taxonomy terms
        $terms = get_terms($taxonomy_slug);

        // Display filter HTML
        echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
        echo '<option value="">' . sprintf(esc_html__('Show All %s', 'text_domain'), $taxonomy_name) . '</option>';
        foreach ($terms as $term) {
            printf(
                    '<option value="%1$s" %2$s>%3$s (%4$s)</option>', $term->slug, ( ( isset($_GET[$taxonomy_slug]) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : ''), $term->name, $term->count
            );
        }
        echo '</select>';
    }
}

add_action('restrict_manage_posts', 'qbpi_filter_quotes', 10, 2);


// add admin interface for map json file generation

add_action('admin_bar_menu', 'add_toolbar_items', 100);

function add_toolbar_items($admin_bar) {
    $admin_bar->add_menu(array(
        'id' => 'map-pdf-gen',
        'title' => 'Update Map JSON',
        'href' => '#',
        'meta' => array(
            'title' => __('Update Map JSON'),
            'class' => 'map-pdf-gen',
        ),
    ));
    $admin_bar->add_menu(array(
        'id' => 'pv-pdf-gen',
        'title' => 'Update PV PDF',
        'href' => '#',
        'meta' => array(
            'title' => __('Update PV PDF'),
            'class' => 'pv-pdf-gen',
        ),
    ));
    $admin_bar->add_menu(array(
        'id' => 'wind-pdf-gen',
        'title' => 'Update Wind PDF',
        'href' => '#',
        'meta' => array(
            'title' => __('Update Wind PDF'),
            'class' => 'wind-pdf-gen',
        ),
    ));
}

//add_action('init', 'projectjson_callback');
add_action('wp_ajax_projectjson', 'projectjson_callback');
add_action('wp_ajax_nopriv_projectjson', 'projectjson_callback');

function projectjson_callback($is_callback, $page) {

    if (!empty($is_callback)) {
        $_POST['page'] = $page;
        $_POST['type'] = 'all';
    }

    $projectsArr = array();

    $args = array(
        'meta_query' => array(
            array(
                'key' => 'column_10',
                'value' => 'Y'
            ),
            array(
                'key' => 'column_1',
                'value' => '',
                'compare' => '!=',
            ),
        ),
        'post_type' => 'projects',
        'posts_per_page' => 3,
        'paged' => 1,
    );

    $args['posts_per_page'] = get_field('cases_per_load', 'option');

    if ($_POST['type'] != 'all') {
        $args['meta_query'][] = array(
            'key' => 'column_6',
            'value' => $_POST['type'],
        );
    }

    $lastBlock = 0;

    if ($_POST['page'] > 1) {
        $args['paged'] = 1;
        $args['offset'] = (($_POST['page'] - 2) * $args['posts_per_page']) + $args['posts_per_page'] - 1;

        // check if current part is last one.
        $checkNextArgs = $args;

        $checkNextArgs['offset'] = (($_POST['page'] - 1) * $args['posts_per_page']) + $args['posts_per_page'] - 1;

        $checkNextPosts = get_posts($checkNextArgs);

        if (count($checkNextPosts) < 1) {
            $lastBlock = 1;
        }
        // END check if current part is last one.
    } else if ($args['paged'] == 1 && $args['posts_per_page'] > 1) {
        $args['posts_per_page'] = $args['posts_per_page'] - 1;

        // check if current part is last one.
        $checkNextArgs = $args;

        $checkNextArgs['paged'] = $args['paged'] + 1;

        $checkNextPosts = get_posts($checkNextArgs);

        if (count($checkNextPosts) < 1) {
            $lastBlock = 1;
        }
        // END check if current part is last one.

        $firstBlock = array(
            "type" => "spec",
            "img" => "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAwAAAAGACAMAAAAtcPVNAAAAtFBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSe1G2AAAAO3RSTlMABQv49BDtGhXx6R/lJCnd4TLZbmgtuNbRkMiHzMTAUz1FSbNzrrxNX1eVnzZbOqqmd0GLm4Jje3+Yo9vjX1IAAFTRSURBVHja7N0Hd6NGEADg2dldehMgkEAV9d6b5///rwhhJz7Hd7EuUp7t6Htnnu7Mszh2Zisrw8PDw8PDw8PDw8PDw/8ZQwR2BucjsuLlw8P3hchQU88HZEwtqdkknTaHQaiWu+kuKwWHrFVCxK+cBdJWGeTwkc3fEKqalJpExPzr/MK2GftYyObn2dWnYzX2qu3tdtSs654gEpyIXOV84CL/EtHTsvlVQ4chLqzKAM5kulqt4eEbYYgy9bxK7PpPk3qlcWq4tZ7nrfbj8S5DdDSEX9Cmo23DoIKgX9I3ocyziiF8HXk90JyMK/rKBmSgphs/RYRfYv9YZ8ivdAu+IVbIX8lDfZnQT4jOQvGeVNTYO8WrlZGBNg2jSKePSnqd3mq2MeGTYoB5dKPmOHBht3tRQyfibpWZm7SEgLbzNNbgF2RYYvAzrFxqBZ1k6djyi7aHXxrL63u1NAy6q2Cf2VILEvpHbuQ2i56+I0slLVOZg8hgpyetks1aNZ1z+hhOQuGiEq0Y3NfvNTJoT+fV8cmUqafUHciTYduJOLcqUaNvd30ivSP7nkHuFH4hqPWmP72uUNEFnSlG3IaH/xJD6WT7NKpYOl1wI/bpA7getVUNzXHPiA3d5UfYLfoDbPe5YrjZoB0eOxZ9hD7ph1nQnQL785KYieyW42MGZ+hUU8e00ZbSRPz4j88qlfTo7Pd1JfZrM3AOjc2x2t1P0uUkarK6ofBKyrKw0XfgVxyE9zEZJjoVuJG24KsOir4kNuzUDU5XEtxPxk4We/440+lCz+TkfGx4Hbc4IdgMY/qImGFTUZTNpSmyGbDm6egmx2onqtr473KbXY6grjUT2dbj3IutJyuueY1Fr95ZqfAhZQeAJZVDc3gMVQi3nWQcbeyktjT8DFR7fcR/7FrCLzg1noe+77kVvxqEZXuVleDh/orO/mof6HQdzpdNNS858WqIy72Yvz2PPsQ4phYRrwNrNVfWwQ4UemHtGPw2GZxGNqrdKoZ6bC0x687GzXZEz7hSMeHjMgeG0TIavNw5LEtQNfiIVrf9qxMHRIZubWan/Snpe1SpEfclwKMhuDPZNmWpKQHCuv4c1/QRor7sD1GiXR4odDM8WjtDI78E43WmVRj8tionbnQOnCbTBZG/jRCZmo1rfYWLOOmnJsJ/YzjprOHnNLdemqsQ/XX/eZWBLLX28JgVuh9sKl6s0wIB2SgWnOudiM54gd4nvNosj6NepW8Zgm5Gr06XCv+x+dB5FM7h962XChVLD0p0+dH9SaCZDCDrrj9VZBUdPZlyrvBLAehDVmoYSlTelNVPdaXfiLNY+JRL1fTI5Gi3mqoVYbj1pzQYDoNNQ6e/441MY3IShQndWOft+yUda3gw4d+pXeJe0XWFCtx31/hpuxbByRlG9U23u2mO8jvsWaQY/Sl81uv9ktjz5IfsU4FbxAeMIcJ6177URU6pNZh1fXpLVHYMYOryM7ox8XaQvQPnBlme+kIkznxUFZQTHYvHSWttw+fG2jrPifzPE2TqIwVupt2LWnC2FPSXeN5OeoCd/m5UkrIhBP0dFwGDs2kS7TtHhe5Jbx5KcBPqaNaaHSqXhOWVETg9QQ1j9slXm2a6sej1q/ts021u26y7zODhRmZjYwcAmsfpFSEoVtUqJxJxp8fpLYXH6f7V5NGB7kbEwnuC21m6QnDKTUwA2OucKKns4TMbTlqXxto2s0BDAE3Cw7/BpJQ2g4KtwZlz5Geu9Wp46w2WnN6hG435TINC8TBzQncjgsEtp2fYoXs45e2VVdcuPb/oFHH3jlUqO7vFSjXDZl1RBFmbTztm+Tpmtbhu7Zl8FcWgrY67oJXQX+I1DOs6p7cm5ssjv4Dz2VQL9h26G25V4bbC2OVExvTPEBtNHLgbHMxmzd9NnWHdhGd2OBZ0Ea1nkjH1MnZDfEwKfdAPzy0HHatGVjuK7cu/lZuoQa77es69ZjuOxPnGpx/p62LsbG5GWlUnxaf74Tyaw41pTnnZMPpwRwwumOlk9TzZVPgNLBvWauxlI8XUogL3BTVw5iXb2dRMG6nzaA8+QE5LYafR6zIoNJutBgmiSn0zAs2nejKdqgCTv6berbRZ1xUvqo72nF4Tew2AtdKFRSKmOxsPTbgLx4E7YYzZwzJqpul0fV0Q51aVwW9Qj7NNq8ij3XG1cukvi/0lHYSRF9Qcrsfwsp5wPrL/wQozMpZWosrhnAEaFFhkeL6hcE68y/DEiRSxAoZmeplr80VtAHX+MuCl1xZwNhCc/gO8/NXKhuEo6PpkLFzLMogbSbQMzat/hsSzcn60VdRCn94wOP2lj1c3TzJI460jMfV2+5Ls7r93P0o7zKYlp1xq7qoOFBgz23O0y5vQIGsEdpRwrq8gD7c0ysKNui8DCyeJRT/iZ0s4K1fHOt0d7zgt7QvVTwxLxwV/uVG6H3VtuA4zbWnPepXtuNpRatFuYrhJTP8g0uAK5oxBj868yrhG3Ldioo4G3xSTCFrklXY1wyM+VuGCdQ8SAEtBvRy61gIA1HHazUrBpbKSstjdKhEdhV7jxzDotqHQrFUMwTlX3LulgluKROXYhC+BSa2dKs/Dp814HDoI12Jr34gVupKw4Qqjhqq5VBDklnzBqV6G74ehpsm5vzSBObAVnCveCJ6F/ARQqiu1EkiNQW7jurXORAPAWq0FAGbD71U3P8S2Utd+LPGs2eummezRnfD87Y3V12igsyThdCFWDK6HqGVdha7F40qsXfc+YC4tIi7yVkqpLSed2udeBfk9rNnw3VqDxA4YTu0sHJY0Bs8wnZgArIzwAtulklq8Wrp9YIjZ28IQa/hBqx9Xn5aJlzbonpTe9itkAAaccpzrXfgNuJxU+NVzZCIeO1K9foaqHHnL1v44nM6Xwezr9DE/DuXu+TMXegza1WYGF2xq/7A5pNibwRiY22i5PU3LDICBdMzhMqkpbx7kF4cSwisbwYkX370fnXMhHPj0cMeLdO0GGfwOmXC6knUIm6VjFMBVngtRA1BL3zH0zxDLu8vDgyODi420w1UbLpyjWEBB7vojZCqbH4blQ8CcTcw5F56plrtlgKMlBH8hlErHu3RIGmt4RWt13aXFqXCfTNAHq6Mz/PQtALOrenG9e7jWZZCPZyuFrsCV2hRViaPV9HmfG75d94F3HSY2FFKl+S0X0rTexiVOgo9lEq1HW2vRkhIBgD0Z5KvFLR/mNXrorg86d4kGeYemGhkr6St0AAS1NAhOy07UWZ6Cts2gKbxoNUV4QzbFZd+qEF5F1N3IEHRjjU/7yRCvsKFFF8karuK0UVsPHE2tRttx1aWP4kZj5zDn2I+z9n6/CBDN/e44fbWrWW1n45ZZRpTI4EezFhTU0WjR6JW/XQrYijDqPX4amjZKLfEFCdettyQAboifJB5CBNvg5GdeEi7pbAI5pgJ2uJijZG+rqNJIg/cMeq5bObbGqabtUQMn7DUMnW4p+gLlMy9W0EWtDNfJvJ4vSHE9us6mbCLO1+1GsWuPJ1GegXqjs4JnLUWQsJRqFD+tNYT32Me+JYQwqt+uIzRsm5o8tpyer5lR1BGUU/oa4k4n3ugQjQFK4wqvGJxfvhswGxEdFYBFlDTiLp49N6xynU1tmf/1XbZqyh9nstcu3VIAnxwru8VQaMd+eoqD6tSRiABy/uosW9BvSRIjqQvFord6l3JCqU1fP9V4gveU0lap3GruB8C+9MdR/tQmbrLs6egSXebrqdbbDPt0UWelJtO2vUmvxy/7D/dRNkkj34S5Qmeiupz0j7ZtjqIoEVw3KtVlCz4IG3RD3P/sizQjg3J6ivAze6XuCqW22djAsrAMYE5thggsptsSybZrm2nFV+iVp/dHiqgFZvuQNp1ROtKAfbsPJ2YII78XC2GsF2m4rNOZuEz+6k3W4EPItfjZQprDS9TuoOy96mgqr4a4PLhiQoTTreiNmoS/qJ8uGxBLRQIsf3FOTAVeAmDxE7C+6za3TxJ2euzRjRkG/eXnl5aeJl0jH7RdNh4n2Xw0G2VfoL95lVmkTpuDppx2u1WPc8sfhrvNrpUBBt0W5NRj2l2EDNJ6hRPVAULSRR78nJ4V0SyOGnyUWZ0IupF4aMNfZH0Hnwtue0V08zX81JAKvMMQm/pJmzfNhBNF2XSOJUH3FsHFj0O7sEavcZEf+lP44tDUVLtsy6IHD4WJ4ILr25WJ8L42Y3KeJpUQwG6MTka/O7c8X8SKbsXjHeeVxNPgGitOt7KS8EL2Jp+tilL/bCTDnzbE5Qpd8Ak6jbpC2V7pHOJis8PBbCl0b1vImUmTMWCqhgjgtHBL7/DNL94NOhquZyhGpdpU0ckcLOK7M1nPBr9oxR0AZ8QQUUXGGJoAOFNlC8slkwHuRshsuAomnG6DK0N4Id99bBG1W/22DmDXf0LEvKe/bB9CeE+7qlBBBKzK81RphzG9MBS6u9qlOkzJCIb20PPTtNWyRMOg97iBjZfF0ctg+uuNjg+cnrkni5Qow/fKmiEUmFo22/36MC/8waTft5aDdfayV0q++v9fGxjOgm6Bx9shg19h9ukJ/j3tuFlo6r4RtvDaQg/dmrjE98l+Lz09+kto9vMTddel/1R/pKK8XEgxSa0Y9HNebzU1s9G6lZ1qi5Gpfa0kYCmn10SVwVtBL0jLkNMCT/GMYiFgVKMcFzy6zGYe+35lMQUmy1lmYjewVfOa2DCVf1nx00UH/sF0s2r9u/26KKeIY9KH1acT+YtFdcDgKlL7g7373E4dBwIAPNLITbiCwRhMCc300CGZ93+vXZsLgRC4QJJlc3e/P/EhJ6CAsaSRZhyZrjBVdCzgHN5Z0psQuEOPgNn+QtDVDIm0LR2A0q3W/m3jzksyAR1CI9CPb7Og68q8iDRPP/9YUgLNMkCbfsEYgNWc5Kjq2ZtIRQy0CknXnJRyDK4V0mdU6yWJoqg6OlzG4H585nkNb9A0502kKBSSSBVE8uYCXIqm1bT8eqnA68TicCRHe8IDXqQfB5vWx1WG/4XZZLxAB9AtPHndl1IZeDqu4wBQcItBWEU/zdce19vp2TZjBydsG4B1jTR+zNeSUitbEiEKtfHPzINFPAo9P2a2B8DgezCrbbR9V+63Xu9U85zdu0nmRZhzT8krfP8ETwbtqBqE9BONOUvnR/xXcCW3jG0Ab8T5rKFxxv89Swi2oD00C/leWxJRWe9McmPoLp54eqHH/sBLP6xQe1KJsBrUOSsRkSmEjDSAobqdOz0hbXVYQ0StUezPrj0flWkroLsF0MbX5RMkahn4Blz3HSLTpBPYGSx67eeJwtnFC9zHNxQYl0wzENWXRbhJr5uso9LeCtr0A2FzueL1cOO3o3atPvTqLomIg6dWKwLdRW6zHsY1z/oX3N+QbehN25oWKSHmAyIy87VI9HlMybGSds6mkAZSwimwhkrYqo3SKkEdSqzBf+u8eUPLr+JB2Rvl4Ro1M3TpbmLmFxxaQiLswxdjmQz0HaTLJvOh1xtzOGfQefXhA7ZJW6Ukhgb5fIV2cjDChED6aSqSUiiEIYgmXQbQpB2BaMhCufbor8BM0o4Z6bpLB9SiEElnYGxnY4lhpCJtSR0GEhdH/UgT8iptob/cNAfp01CVwxUagj4ja/E4sCDBV0exqM/jtVIuY9LvGQapfTudQp22wFYJg/RXusX23Ypi8bd3zXDXisIhziKlsMEzudf1svzkO/SzVZcW5zV6B58bCofHYQWJtIUzrk1UOmHyND5p1CFlT0xKhRwUq5s5WrlUMyHu07LoTfWaWg0ZSZ9i9qc2OynuMtPg08YVgbKq0pVksc9hh63G40w+bZhViQZDAN3mL7IBLMlo1Mtu4FheKGgviAra28wsG3LoDdaTmHUF/XDZ5SJLJ7DUf2CaDetZC4n0a5AzFVIg0jFsvR6HF8eSkpQXDQ6MaOv1zMbOyhPTbQUuyksitYL0CT6HY43AnLDPzny9smE2kW7RHmcyHFKzhWpKM7cNgvBhfdyRC5WqWmMyyGKAROrUpWOFzGz/kGpDn4hE7SdGg65kjjIcHseKiILA3QDkZ5nea8WkD0Ww0xNi+q6AgZYVuK0CMTyzXLguqhUNLupKOWd3Lghv660/hzocYZqdh8+JO6/Pfm9CtxGmWeYMEswkQsolR3xuCEEJvLiy5ai00/oVFl2HP74HOA/NyfiBa8gjEWlc2SUtlgR9yLVgJ/YVeMfq9gsBEtFU0llz7WLwi9k2Y+2+pDu45Q4lZvDF+DIPvCzodqLT2kV31WYnB2Bt2nQrNQ88TOfHBv3RjGAwY/AgPQu29JJAIkzQiabN4byZH1ZEOuW9oJ8b1Tmcp3StWqYWIN0sC5nk1XEMX095MegurgIALFlE5MnBM92sGAMLVfpvULuPT7QZ+69Bsb3oL54jiScNrFrn2+er9B4KegcJxTPncM4GTUNubEm3cp45CwVW1Bi+XGZCVxESkY7gKJ3bvFIyHFJ6gm6EBQVY7+cFQO8VsNpLrGR0eCSu2fVGHMeNXpFOmJ3W2daNm46TFXgU4qoinVDb5xMBtWlZIjpIN6qORlas53K2P4SvZjt0HZzkBtnj/5+D9sTBEoSDddGg24jslMPQ/69c//9mWAuSRbNoM3gY5Tm7jfSjinQKqcAubPFSGptOYKqGYahuVlBWGZWQTqAH5427HUfQjaSBEp1SpVqHr6Z16FrGi867lSDIGoiuFEWf8XxJtPNrukup1/U8SX8+VSKlcBFRws09biTEC+JSUwelEv9tF2KN60NL90yscmArl074cBGP6C64iRX4aoXzUadTTqcGjPOndU+3a4xPQ5PSzRP3ESRd+vMZcWycjP18eJiGKcS71QDsCEoEaw+U46LccN54pSc/PIfeK+pw0YDuIJo9+AZF+oBaybVakUqn5rC3ntD/ruAoMb67oOCkd1OdsK9lzeJet31Y33zBqiWBiFIWOezxXCWa/36JlzeQjhhZx4OLJnQzURlx+Hr8SdCpKC1UwpZ0QrX3W38Vk/53BcQR62fbi4VJW5VOdQVX4/XliHPGvnpf3a+zFt3WdNBioLDcxp8XnZDDjr1w02qT8BvrRdZEOuBkLrWVcYWV6TZomCsFvoEWmnSqmr4Wizd0AkMAiCfzjM4g94dH7r8Eqm2fA/CD3QSDG25aYw9fBLm19rS1WPi3Zqeyy7ULoigqldre8aMHf7aklMvhsl5d1/yj0P7FPPV1s5QL6AYoZDiz4TvwV/rAtjwMawmTTkmNwxRRFmuNwX8ofnOfSHUPTjC7mhVp7PjKzDwt13FVY3uH3oQor2peRuFJf8CuGTd1PbhHrEHKk5QwripFrizF0bbrM9kjDLhJV0MiNDq5mQXfg8+RPiAWaXFPPJNJOFC4n5P0A/cv/+OSBIhDvCuFkdXg96zpquXQCTRkUCospt14bFuKcu4s48pwVWP82fQAYFsZj8G1lHHSQHaQv9XUb8m9T5mFTqgzeE/x7RHSlbDSLEXlJ/gejIHyF3tntp02DITh0eJ9A+/GNkvY9xgcoNH7v1drmwIuGGiT5tAm31VwdMA+Ho2k0a+ZywI4EmIAPJZZFa+tdv1f1y5/ELQHZfRhXYDb6ObNaYEke1HcD+schxHCJwczsThMI6P5MnxVmacAfnE7MJu0lHsnUEWzuYacvkqK1Ab1k/6DNE3LfvJsgY6arMxgfvaLQ1arsTuhaf1vHi9t72bTyyK3tQhIW9Kr9/Y1+bmTGvoj5zRg98L7nmsl34qhQLfWrVngGW4yCovaIg0He2yDB4yo7nZ1tx4PKaYZ1sV20KdZB4imiQ74p1ZaVr1uslf82Pn1gjH7Bbk+hzKaSe6vBg9/D4REk0nsHGKM9az7x+yLd4H+brEZjLHY3pLf/Znpcx1jHDJWk9Re2z4eOs2OeezcfStzaa2U4nB2mVKMCSniJsr6lqWgn6E+fzwLlbyJ4Bffu8TZh1fO2ULBedCQyB7mys+/Cmp3PlsX/h54aMUV2hw9j5D9x8Lkj0au/2ZZqO7SoOwPMILJq5UZp9VWrwq6vLg5SE+FqZjj2ml3hIoPCPSFNu37Jk/VUNjs966XOGJmHhxx9nYucwA4HAEnFmEjNKeXBMMNG8p07nOupg5v44oiDwUVnZCEefzzU8gT3gm+FpPqcmU1nrEt/AbC9I2S6+yW+HvKiczhwK7pDqYbS9MQApg7IIoAeLTejIYaxz2ZLCO2sqAWAlBqLEcNcNFdAM1WXJE15VJuP+JzUKJt3neECN6GtgyrzF88j/2UA9SLr/jOvZCWntKK+H86GYntSNbgN+BCX/6I8NrgSeM4ThQETRGwbU+bkaEa7nbe1nUhxyVMnk6EcZcehc/SHAFan1Zp4aw2wIqn3kooRUFT56dKlL72oMw3dhvqz6p2RsofqrH8bmXd3WqtIuSIXyPANYgk8QYt9G280LncxF22C4vW4PfAixefsr8MaUzXYbheNj3Tc3uKIB3u2/AbcfMHuQn0yq6SWhgN+dM1KlIwoC5jJEkldkQ2sejSolkLfqHO3974CkS4TH3OHXfsFlvhilQpWMNFBINV0UVQ0GBfVKI+W9ZYX04zl2jU9PmlSEJsvS0pq0Q/cpfFHA4uG2IakPKF5CkqHYbK6PC84dbDhD82671g4LI4IuX9DvzKrroH8IRIEo1SqECYSBMREId0bSMoU9ZNHaiiXJr0rhJEGyjAXzGgaqTkkAFGsoQFXp63cDUEb0EUgtdnl1LKPojmtsoXn+m6s0uyShiRdwhyUmZsHa3lnrRV/U2mMqCs+8TBOUml/bfc5421FisLbzXSGntGi6Cldr1+bOQ2ew9YgAOaxKp4RpBT/1oDVEItDBlOJEUvgBVd/bWBN+bgLQjtaUMBwGk3JuxjoAa7F9U1iLHz+XgMBUqTMZJ1iRI1IXsCuoRL1CtWTYaP4Rp6RCglqmbzJiOE5cQOBzfB21c4wJnVD9dyIMM5eRxKJelL83ZAOrzSVoJBTHxj7ymp2jAlwog6Ud5UkUGYDSTGdzRHGbsPtBQjZD8p8yyn31R7LQR7UnaRwHEArPVlpUbMLkFbGlxlE8frQDV2Rklz0YObzMhpo941T+DkN9geULoXngRbu9VT1a8xIccbYzjCPdO8XBY1a/LEwaIdMb8Ob0BcbBsyyROwy+ZD7cUYZuw2spmPFABgM4YDFs3DAhI92wMTqy35z7a+MFb0Cc8oK+HCTbD7VJJjXGGf41HZTmVZXW4cMU90nrgP9TY+mON6tJyRQE+zS4O+ka6i4h+KOYK3MPb4h/U0/FRrNWqxDVgRhhPupPqGFz1b403qUVYmgSrmVVH4G4zi8wAZXd8x/5+VqkrKrBpVgAK0aLfrgvbkcOJL95Mpfwgh9AeE5Sl7zTA2arRYJOnopPJcY19rrC1ybShw8BvLKw+N4108Wl9o9OPIBmw3m4El4LMtV3GnlpOl1DiooC39WQeYJ3ZAWQl+CzewXQQF5zEeap5liTxR7rpywyC1tPvZlgAkWjanL2nqm8so6BodQLrTzwZ1ykfOsTZbbv90DO9LKtFs2UVpLX7AWJzRAasonWkrF+zaYzn+LrcrqkMFFQmkXuEmSOyXNgzot1vCVryMEJQ5CLLNYFPuAZJ/fKzVozmgj4K4TiFu6QwBA87fYuIbpt/eJUOxLH4nAYb3hRuHz5PxeDLVBJc9FIQQQ/tZx5wST+fwr8fOHINlLIVF9oekQxUJz86p1eEm9f6A0oP5u7uEu9JXEKwW+vZlfjaQSCS3dmNVVrAS3xbhwIx+znk/7WI4ozPTRKGkFo6KExsI3gXxLPsCAhSyR6IWD+IRcC0zd51TmX1bJ0K+QoQj+9RoUaeTaT53UAU+V6OR2gJus/G74VQqbihKEVyh059pqlzLA9dl8LAhUWJYCgCc5qjzhVKr6fNnHAP4b8ptKbm4H+xjHb2P/aej0BI4dNS44IUg9NjjQMwnBAjA8mKDSfIYhqYneU6eWvx0DAh4mjvXYNQ1TQsqWcWsDD9I4B4wALZ9VZWkIb7eMPJ1m2VI54p0xXrt6oWWz2YHXn5Z11ifsAPQAG4xbC1dg+WknffJ06TFYYN3dwrkoHZP16IoeqS1lzSHHEVBQc3OfLXAPSnZhdBCChwZvhr5BKOLS0PDTUFCk0NwN4Kjj0c32s/DVsizHH52OWEAwrYOnFFSQhSIiS0APJIH+iCkUIRbTOjJLqFch/cAAWenyQpyZjIzvIdyPkQ6UWdq39m7r63UtSgMwP8qKSshCSVBuvSmKGyaON//vY4EcGMB4jgQ2MJ3Y3SEmxiySmZxAdN2Uggxt845ttndlqeEHGAv9mDRNsvAcRWDotyqYPsVs//MxQjo6bTWwYZP1Nb4gq6M0Oc4iA1oS5/j2Ez/kh79S6qRMT7t4tx5YwCmzWB2ulVb0zQ7xbFh+41EGzAZ9ujTNpnCUfG5OaC1FsM3UgUpRQdAhVZ0B2vm8k+T5rUtgkW2E6WZmrf9EZ/hyPiDfmEXXm/ik5SlCimADXMmFkKo3MN4Wko27Q/J/9CqPHIRCdIdHBUPYCQptKOP8YSIpG8AtQSF2p/eFV/UIBwDUTFx2B9F29QfHFn30q67muEze97BG80radjaz2k47ENuTwr7uB/KSmdxdForsS7Bv7smbrIGIEyjU3oHGwO6QrJtIAL/45xJznBkRSlot/i7yYoFdurVAe7Ru1JV42BYeSwPGfaxc/RO1HF0LAij5vTswsQ3pkSUeFrH5iXu7wfY+Dc7V/8/Qg21aMX1aUup/hrgyIxJRcrdLykWufCnkhSTto192Ich8W7cdR+xZgbYr6YnLSU3DSyPixkcHZVN7n7FPCWSUyx1pMwgdLUJYaLcYwwRzGiL7OLYuGGyaWFo0bf0l0wYDSbymYVHsUiksJfRtOgDr7EZBLiJ/dgft9ZbbapNGI7Kvm/7julXPVrSDXyVSlCarbduxzb+4sy/tGnoqcmybbuRtvTntEW3cWTcrzQe20qKb0Ph5MhQFJoydNJJKU4/JcrjkGf6qAujjqVuCxHwvEUkHnFcRUkq3TJ8peuCdlTmKJD19CGYg3HbBPisl6crMzS0cVF/xGGORVueOY7LKelyzudK6s9p+kyOedhQWU9aCQfQgmbprj0teVIIQacywm4a+66IVAZOD294wbIRhZ9Q3hOOSONmaV3TtNcJ268XDI4v8kQtwwbTXHMVO1Rr5p4Nu6kuLg735ArF3uPjZFknhK0uIHa6214BlzQcmVYrOkBzcs+n9JG4GzHHEtLza3a1xrDENUCzp6VWmQSdxgA7sX4AgM/F9x1UZoKyHBEwZ1DDEfFppr8KR82y8ZMW9r4p++y7/Qy9vbD/FK1nDsOptRQRVe7oKomp77PieJjCm/wMu7A0rclcbe7idLqfMgLKKYBny3MHX3CMciVxkhwCkcJOrHwPsC+tQQs2W5W4C8ePmG0nvKihaywM/NHDXx7xgWloGXrTLitBoukWvCtLevlsPn2qSqKyBqC/iPAFmNg4qYGQjbIlLKkrWvLGHHD4zsVzWYWdrejIuqbBd0YzBN9VEZQvHQ44GStsFYP4bbLeVQBm28b8a7YNN+xheerRO3Glz/0tenqcCHe9bRbkC4e7u4kqTqtbmZhaLdd9GVm0pPsM+/CRpadVSwk6qmSj7WMHE0DVE/SZPjCDxrqQkInYsVnifQE/8irpTRkjg2822prlHN3skHU93TKxS2fTp2KC09I0DsDlmKwWHC84JKjVu4NFTtCRlTj2CL7JF5cNb3PU54idWaBQHxgpWktn2l0spZ5ndLPbg62T6GKXQIapQ5nqDPHgWSJSlXtEpBXoyOQ99mFD2kP4iBt/EBTKAEF+K8X6TsObuupf3S7PTzRTkmiMXUxrNf+JCx8kiZI+R1TslY6txDRXw05GgvbIccQsfOoLpedS4EGJ/lIuAD4RV77cPaCRIVIp7BL2AUrYiAfTRpJkuo7I2FOLjm0Y5BML196ZstI+MH7ErEIkZXnwxwBq2ysUr8kAnrmwWNuL05gSeWxve10xRzy0+7wlkhMH0dlJSUcmcomw3ufiS3gz5yY7FDuW9BGzXs56yBtYqjUbSV1QyKoxQJvd7v/9RKYnSHf3ldmjO4YYMKaNFYmCi58wyh6dijVi2MbuX56dQ7mzDZMhZmYVG8xoDhWFkhlXc8e36c8BJU0r7VsDwFVyjBho952mlCrn4keceeF0qRyqZ5sa/npQBQ7mPAjaTbQecT6PZSVXBW6l3kjl03RzQA8o0t5n/OuY4+SYvZAqkX2eGfgZzrIkC5JOQ3qtooN3jz0H7KlE+7VMhvNgbk7K53Syn/eboxqr080hogpMwvjZM+KmMSjJUsHKvgT4sbwUfkHSyTRSbhAEBsMbxs1ukg5IvNYNgzPEz3gtv77CcBDiV1jl4ed6QD/cvT4fNhiWc+WFpj21VBM/xl8S/epCp5PRrTcFB29cvyGjfKJQ6XZsxK46Y+9VfnhqekHl5i/XYFUOps1wNmapMXZWR3WOn2P1DvhI0ElJF2/yIvrkqYuz8Uem9nK7/SPphF8AYdk4G+bg/4in02fCWKf1RiFUOl/0OcOZzMVz/3b/R6I7QJ/SLWuAf1uPTqyHpSlFUW7mW1J2OxriphlGoGm3nl+RVQDWpgwebfzbgqSgk+o4JhheKAK91xdEMin7DDGbtwuJ6bh4e/sV1QTgOfLx76uW6ZRkvT3hNvxoM6DNQYB4sVe6+YlEDcBU1PELuBadkGjdv6YYNI9+QLmIWT2hbtOf6FQdb8Yew29QtOiUmpyBsyH9gKghZlqq17isQsOXTAyx1PY0/AoDQSeU8DlqQZaik1kDZ6CV6CaSdIClIeXxK7AsnZKY8U5TRj1ZJe8yDGdRu7Ry2xdKdhFqiIKJX+FR0QmJoZ/1VMRzCyPHwLlUbpOgKFocSyxdyY3xK7Bi8oQtzoUQKuqpbY4zcm6ToAh0GyuNIPglcyDASD0nJZ2DkEopQeKNVG0bZ+XTPvLSOjKchVxghQWc/+uvwbbZE0Xxy7WHfreZ9drZSrY44DivLu3h9evp2xxJPDOsuLXfsQv611xS3DwbIRO4hKtZ3J+0HOjZqx8DKibWXlX9Ev5nR8THd5JiJJLewLRdg+NSFGmPDLSHS+tKHrt0ChsZSrr4ZcwHQbGRLTeoPnjJ0uxiHiQL+oZU781pOPzrHgLk/Yfih3nOwC7n+fV/cQaWo7jIvNF71detTy/DtwEbatRMS3rzqgHgV50uLPMcG6xFlGh2jdq/HhD9H3n32l68EoUB+JlDIicSgqCOVUrqrGit//+/dgQvRfvua+8KiftLe6l+aC+TzKysmecg3ypkihSVcb8riEjXc20NN5NKcZ5SOOOcASmVTgnjEyh8ht9+KgD8R74FFHGwC9esVEdICFaShiUpInZepQ1/0cCtMCVbr+VX3fdpIz9sNM2WpD2ZC2PJ5HjAAZjqn+sfLxsPWwrSn7HHtaykkJghKaYUHatXpZBR6nLciOnbu7NhJAmp5xza09fmqP9h6B8Moe5RRnyjIx9tCIRH7+tWGX+8jpcqBQSJdyTFiCIjV23aEY3DFs+ILQV9w2EINPMI8DTHUN1nrm281B6rc1TO846uvmQY9vh0m7clStV6EnbFbKVViopaoT0xZwgN6hzRUrp07iwvMm+3gLKkgN1AiKUcehz62kSh94wjLWlYXSmomuJ3U8L7Be90C3pWQUBpewoi9vHTuU97JXIBtrIoYGS5piBQfpy1sFrGmZH+/swXNTlFogzpJjxf44zndb+sIVpFukzY2tGbZBOBV50C3qLzgoDy+SgjwOrhHPesV4D7TSRKn25BCuoMR+7YrtYURGtNF8l6HwejXffLpy633fAcG4/SOfp+OVwnn03S3GfnhSIndM/3DBIqyVyPI2I9ukA6HwwHXGkMsMHcNycMyluaDGAe/ZGkFbGgE21cwFuNdeL6IIAxRU3U8qZbcoik/ZJB5J7pgpNU22ffWWNnqIpwCLxyaCrtCKPk2QmpjEqvaJ28ssYF7FMk4kyUs2zXSEmn5HOe14WVa88zuIElnRGeiQ2GnYagnIKdVpuIdMOZc82gDWmTs8wqZjkReZLiM83LdMxYpb+Lu6onbg7EShQh3R6/phn6Dtn9GcNNTOmMNwjLnHygYSulHudfNh1JHfdNHbEObVhDNbcEWKtC8afWFDSNoxx1ac9xUUEnMhLTA7HHKhQZYfktbLxLx8WN8Dadki4CWT+7ej0EypfxB2vVaMG19QAZPXz/8MM1AaWUgCmQushAK5K0qpVdYFRxxnFRTxKRj4TJqBSZ0hBbg/YSt9LU6VSFAwryVkMzsZWV9IQjeaq/AgxaVVCglkFAc2yKN12nMYCWoOIyk2osLCJSBz8eo/CEhHEpOvnjJuxbWdOZYgpaD2yoYW+g2h84ktbFGzZaHm28MwBmvU3xNilXZgD6zjZsS8kRUa7FcZkriKwsEmZJ0VniDnTpjLPizTcc4/6zhiOKtU8jX+oUyDU0QGt0KN4aLIXAdDHCRl9SIJdnuIRViaiDpMlKioq8i2NVXYtO2WWmNRgu4grAeM8oYEvZ7p8zhkCjG/fmoKXCEOAIsfEh3uGbIpD6gaQpSIqKcAt3UERjCzqhNvGtwsu0Oes/Fzv8MBvapwWNul7MWyNK0wYHGENIcYSkgFjikpUg8pN0JMqWYlMkhNANr3wHAwDuWdY3vufrZFtq2U1hzwxXitYMAC/E/UGAcFKs0H/mCgPAyvWOQYGcia80DWxmE1FydkIedCgCulXtrldPd/Eg/SOsef8h2yt8rzfRJekDHPGJ1ErexEaZYs5JDeqq7a5aCCim8iIoMMVXbg+ZHCWxCBpm3lyf+tZI8Xs5SMC3rUrRoL0ux094eeyIIY7kVarmOUIN/bSZRsRrVmQ8VYhIyg5CrGVQoMrxRccf+bTxicQxVbq+Nu7IqNlP8fewu0F1vOoMf7OyXnGkOXcsji3TrxhEZNdt2tA9I+dR7KjdcR6hdElSQI5wTOm07fD1ZKTDfJV5uAEQmjki5+fLrYyJv0oN8ZVbTx9++GSo+lDpq+EJmpmVG79lgb74c3vOOJfCSlyVtqx7OcvmF7G5oKu606ljr9jCf+aa+EOZrt41sCe76k0UYEhx432y0wqBXsARzaOd0r1MY38Tt+iaLEtI2cL94b/7u2yUTnMALxQzepPhoCEoIGfYYGYqlR4U+oJCguzkVUGv3g9d7hqV9j1UP38Wy9aS3yA7DEcmtLHLARtNyn7HqxUpJHPqIol3AEzomp6q6ksBj6NF8VJK44hmU0BOEOCDGu0qxmrwVa24k4Tk450o5OiKPju0wgMZxaxFepI6aYwR0imaAHivRKQ6lYpBsqM6XX+hpZFMPYOup7ZS73EFcDX5mA0AtTzAgWJZ9fGrhsAwp1ulp1UhUyFZfmppgwTfx69ZCHJab4mcOH6nRnHTxQEfu5whwLT38edwlqmtXiWVkrcT+KvlNQfAc4KvHOd4/PbIGBoOFIR4az5iaIyLVH3WqYeEG1QEXYt8eaASEFg/frHD4vnCn1EVXQ1FsowFG5eSfwnLWHQ13iPNgDJx3CEwYTih5XQhXHhOb5pGYte+Ed24i8msnV3WoBiyTZxgk7VKdTZx8SCYr9O15BJ2lOQdnjP5P1lljhOzvEFdlnqcaxdfC7oOkRvgcYwphoR1ntXsU/KXvl+kc3QdTuJOUvqBaVAMqW/FLE7ME9n6/JM3EvT7pFV/kDWwpmmp7DxmT8F2Jq8mTryJagJbn3/ilqqSfl2x33yMKqjytJjPK/H8/JOY49S0mpwYvH+Hm+mOo//y3NJ5lP/iP+zd51LibBgG4Pst6QESeu+9FwHlOf/z+hTEBglRBOK3XLOz+8OZnVFM3uSp7K9VQHxiHwZC2T92/99Sag+CflG8W/83bv9AKkYBOOGcp64WtMg/8qB6ipan3yP/bAUJA5iuKQhu5WR2hcTCd+9QoxzGK6BYaub+gWRXIDVJv0W0dfwljOmaZmlWJOpW5qnOY64ZHTMEZM0jNYOIiiZ5MpMsatkURrL1rxzVp/A4/RJZ/kvxf8Wd10eF/tPT08OkXVRjJZNIbWUj3xw6KtYmeREFBrBBGI8AMv6H405+qEC/w2iF/f7P8YZF1nnbVA/3xqTwDXOVzJo8UXKjh3JoxORfytb7s2wiw5Z0rknYf/9TSQs7+ng0EXSEPf7uqhGnI8mT3P53Swqh/93Q/zMMiOKrwblTX0O/TI2l5cN4nBpqPJqLGXSU4+IbrE6a4gvyMWEAIiGsl5Y53O1VJakaqm06ixr2EvKIQRSLt7NP3QZ5kSt8Q9M2nYeEQ97UKAAWwonS7bB/WtfEykSVn+zNkFJI2ssg5JL0yiAvZkPDN9TLi9pI922uazIAs0nYegaMf6lc67QZUQJwDQpO2nYsO520WxnjdU6sE+5CWp7K0ymioONblO1fGfKWTX2aNChCEhH6l8Y2BRBRacPBynSaNExJUpDaKowjnHEWnZKaly9f6SG8eKrbppPEoDLE9/UkeWspeMYLpqBn8XIonobsf6zs8xQ2lWqPBWqUL/dnLWdSEiTbHFvzTCHSFkSUDu9dxUq2DQpCbShQLI0dZMzgg2XIm0gyvHgsZQ3T7Ftxuj05w90n0ZbhdKDH6BR7zsA7eipjllo1bLEo4BaJyAxtGFR5kBSQOqs/bnKdiKvjjVatVGoReEtI8hZTON8l3gar5JCn6faK9zKgr/hAxF0syJ+IJbFTnWn1JN6NVSIZ2nWaFUGBSVMQmZNSEnvRp5iq2lM3ojAcx/O+I0jmK7xig1D0Dyxw95UWpwk7MeBA5jvwwGJhWYl6BOuftd9gQFv51mzEGcMe43izkuTJtiYxhp2aHXXzjirpE0PQdf0zje/BcX1KqoamKf3a6CL+jYFLhFMqRt9XHEaUL1uGhZrojVI6A8CsWmWVwp5eJm+PJZNjK1KUEWiVwiafLapSGLaZdUwn81SkqzJDe1TfzvAxQ2IEliiSF/HE4SnlEFE5pG/Bj/QDItN4rFoMwMPHGJjRXoyGUb0zNQ3Z4Njrkrd89vUEsKYk5nihRazcpP00akY7iVoKCUFXZC9D+jnd0jBORGUNSEryoFrwtl3ApYa0wrxAP+Q89CxmlekzNZ7O2fRMVrE3840cN97HcMywx3SGV9UsXY+8PwAdU3cEib4OqyHoOIfDx8oI77Nlgn5MnXZ8qpnX2KvY5KOtKxZn21fxPMcRdUFXk76HgI7qlm0hHznckkFH+AZ59jv32wilJp1BJW857LFuhrzJZXOz6uXpmdG1FBywHLoSGftLPRtXpXeLpNYAz1TNCn66RBQP5cOlVaIL2eBdVSVvUpLx+nV7sK5yfJWIC7oGtRHmfP2tpUxaAJpJx81OLp1ph/ECiG4EXYht/WjtVLyDA26RriEXxk8oPEpUAJSGpGNEE77qqqggbJgybtHlfIynRA0KKq/jQDVjGAZdjKQXdkjDFGHRolgE0KaCjii68Dcth60eVJvX1xm6ILv+o95qWeA4MB+0YhSY4Qj6jqIqY042pFGK0MgRNQFoGTqihxMiYdsoyxa2KeiiMtqPpuTKMQ7MO9suTdWgIDK1PH3HptEY1u73/xPmNhV0ACPV4wIIhoXkQVMx6eJGeLOhwFQXB5I2vRjEyZ9BJG1zgiSdJN7+NWbVKO5OGmWKjxxga/vnLb/crXTCcaupSrq4Kd6kKbAWwwFephdNm/wI5yHuNJJNjgqdlMmqcSLVaTuDkNyTQm+YkSOPAt8OgrGmxc0yBGMmlVqDLs/Rf/AIpI5wRM3e5mnz5CffYeMOx7O5IH8iXneXBTILFaUe2kr10KnILNvdjr4QVQSjV2raQwgm5FYcugKRxF7i3GDkWiWjoW3Izwx7+omjwsgOATTM+4vvt7BShr++DnxRRzAKdLZ0b3/kbugqMtphKsyfVMc4iheKfR0L8rMOmniwn5J4Np3iuNt/QCE16mKrKeizSsARsxHMhgpzFfxcuCY+nrBgb0EnlYJI53R4sE6V7qk1vBk65KOErbnHaeN2FNz54fFvXwAs4nI8y7SYkoiC4ZaiKl2HOsfWLnYQQI8x+JiSJ2EW8EFSkiczhx0Fx2gl8z4a8YQWfTZj+IIrDB8pswTHs40amdeVqMUZbkZ/oGsp6Qe/kn5kBH5Ylryo6RnHB3xjeu8qV/wzJGTcJ2P5O+jSiOv4iOlaYtarMbxjFgAGuMVedWYV5hGOW+FrSdciathjbTpJRn96AUw4PmMt8mAz+EmpNL1XRZ8eFvSJGOMDVl/2DWHEdLxh2z86B8Yp9lRYRjWGW+kZdDVyjjc9QSeoTgq+nsiDWcNXDfJg6/DzQBS+uq2wOWjSyOGDoUkvSgp23PqosG4WKj2N44WWtHA7UZuuaMOwxzPkz0i6DL6SdFy2xvBVskQ/ad5Alux7ZuCUgu/k5B69UDvYqZaLpVbZNjLxtY49K8W+GQ4NQQ/YD6gJjr0F+QlSMV75xthe3pV03FKBj76Y/rUw6LiDK+vTVw8HXVZ5hi3eMuzFeGGoROZTTePsNdTGOgpTcHWpEl2Vsw42gcLIpys/DF/JloIjdIeOsyvwMYz9uRhQtcksC9dUtekLR/la+LLEzqi8bKY688JjupyJlae5NcMz3YKlNPsjhitbSLouO4K9pEGe0vMoC5CKpEPmUwpHFclDHz5Y88+9AvMoxgWl3q1xXAlb2N6ncMqhF11ssWSFoZNTOIuOe4knKc3V7hBQLP74sGAI7E8lgT94xJ737ENBSwSxoAPyUcdxJa++5XgFPv5kFsxK6IVYu4prsQ6GBMXrkV38X2/ITyPGdADubkNwKuGWhIj31xxAd8l0a8hwZUlJV1bCm7qkY2T7aVr76QuMo+DA+0u3cXTXc+bPPeWcxFitkXNxPfWvrdrFhquMern+UtAzke3gQHRZ6WyyJM31SkMniVuwJoKuK4s3vEiHRH7hKhZDECP6QsgHeNj2YZYjeToi/X8M9ER4NIXrGU1USZ+0+vG4lDa9EH0NX3FoLnPzqipLrSTTIxqHPma4srmk68qeqGXIzDmCGgr6rLxI+cRMRb6DNR0hFxq03qjy1+I9/pTJRsf1WLOcTR9J6b9pcMgBuOnlup/sdVivPyisB6Wr5wRWNzwB0DprJ+PBWA6R6XB4WZdi8TkwomPUp/FDJp7+k8/7njSz6OKalKYTeC8kZ3A5AH3IoOBZbRaXqiBn0rFwTX26shjHmzJ9JbIufPgXOLQrHJ6G1VViV9pwjFGWJJZ/LuDjSyl0LVwVT5YMOq56eAG8YLtsMBuPGR5efwMmLsBwLT2VrstoKp4ngDCnHfa9piR6JzrwxTgAHiMvjoYjeDiaVr2ELIBVzRXld8q6aoNchQM8114m24LINGKO7GC0jOJKeJmuzH6/0a6/fGWSsPAtSpzeyRR8+VfQ2fkZO5oHaCST/69Ho4vi9VzRoBeqKehNScExrN5w0jWAdyebQnoTE4XlKrmJIjEd41oGdG1qAq/qnzO4zch5eYzqGd+v6Ne4RwLZuQ/J/Q5eX+RVosyyOTDfl4qmhvNOc5boafiEu+tZdVcMFHG1ZUPngMagXbFLrEFXF4seltFK9SGC7+tIelcLHjo9tPLeGV76f0WHLo7Pl+VGhUF7VN9i2+lyPmuqdjyn+7ScWilcneLQ9RUOeunMp4KFH1Ay32xFRUrSUWvv9IF574j5JjZM4Rk/rLRRcyF7nuxKur4s/1ieQNIoNXV+fhQriQCULJHIZAV9scExrCq2fTx3P6Id5lmNkP0wp3QDqvZxQFC5X6gy/FCS3i2DroGyKxWDvkjjmEpTkGiHbZLl3/G12jh8CyLbdANyjp2EIezSWOH4sQq9e0QQrqQ4XCkCXQCFjKDiGHc/Nc+IUO8d1GJ0C406tiKFaWKMc7iS3nQRhGaQzd3iNE6fTHDMiojWuPu5yqCRkfSuFaqIgp4TdAuyWN9nAnEeJU97dhRBWAbZilKzEuZBG+uhsSGpibsz6NFeziFp0k4fYZLK0G2I4rm5vsPN2wOGIKJy122pr+Xpicb6U5kmUEJ11/p7lMTkqU07G4SJvpR0I+pEx4GzjoAmAtFUWh4ONFOrOMraUDzRX9etezLsDMzSmzKMJwB0m25FLtnvbrbMIRBuUw9bZUF7dkHBUTyXHrTjZqlwzwWcR2sRUfgWBFsm3Yy5YjjDwcaxadATYL+mfJmlV85MwXGR1oOWGJTk5u/WxIWEG6cXIdu96Uq6ndjxqYTcshiC4xnaeUAg47fSBqtZFEQknHiSw4OeN1NKdd5O8Pt7wJmSKhHJKjxFmho0jqtKGXRDdhJHuP3vbQ15oK1SJ2jmIIdXVr1hE8nZCF60atKsAVrTTRTCFcD+e/iTILI1eNGnZhXX/inrU9ugm1Fz+MRauQAijysF39ARu3HoDIF0aYU3bmubl/ZUS2vriIZUvzPLPODu7DHk6hBHcVQXajrC44t6BNcUSTzQzRSXHHsKtO4kVv/BRgpL3U8eC2RFa4Y9libhU+3MmxPG+Tq56najhXjIyrj+nO0gs4mCQ270sVlS7eHwUbWLrTGuaqjSrTjF1GsJeaXbT2xiQrg/LWh1lghoQZke9phjbHwKkbq5FMDyTsHqJWate0nQeVKqIDJm+Iqt2k+mSSQ7aZWeDbSoG2UAH9a6oxQuTctJupklZ9FkYlHMxKQtiKSL72NtIpmIIBiW/zibiMUnGrxNJgCUTD63aibL83sq4DzKoOwQOR280DX2oa/P3I2TM+mFvUm302sttSgXYxMXF6c/SLqVWO6xYdvGsW3awY1UinEExB0iNYo93/h+LVvYZgtrrceeVb4HQs9lzZtx2hYX8spmsqxiR1dpKxZpvG9tNqcNg4ji6w7DpVktuhmp0gcD/ISSM/sAIkMEwEryYzZGhzetMYgCYMpsUn9qjnr3XNjZeE8larvDRca21VIVWzVJWzG9cLi7XM3OOC4t1aZwWOJH9GYU0B7TGk5j3XLAfLzWb1jY2rT0aQKDx3sq4HxxItEuGyKXyNL2A2OVMu2IXImOMKO4uHGcwsDo4ee0vsjxQFeAQQ2cNlyk33Y6FNhYq+cz9yPgbCyXLmZMMiYaVkIUONDL0J4q6JjlkOPSnigEZF/DGR5JrSEApUHOWMEJ7KlfZ+8j9yO1khm2ZqY/SYvWKi0yx4AryFwP2ROdYpcXY1xYT9Dt5bVzh9y1FQRQjVP55AwK/j6khQ2f7H5JEk3uz0C/opaXMyBiE5nlQHkoWarhsrQM3ZxI4CxpIjFCACxBJPscvhjHXrWlqg49i91TYb+jUyoArCCIyMlQEG1c2MqmW4tpOEueiEocAWjOdjyXr5SFvcfpZJmnZ+q9O/h3sGoUgJalwEyX4aJ48+bvwYtf6O839cBrDf2Xf/FlF69YJWXxAVEsFr9fAL9qFRcUkGwndVwU65UF+TBUW6VLUlO/MOOuzBHEUBJR2YW3WryhYEfvse0l01r+vX1h4cZHE0dQQE7OjbjViMVwKeOpQR7M9nQxS6yLdDnnv2GuzYydQCBVsd9a7EFpk9HT8UzrWHUA1byaqWq4+11WwqagjHajXWq11lENF2ItHDokMvny0tU4gLpJF6POcSat2fOdLj3scbyq+J85LJIURMXBnAOdgYZnrFbI32shfh/P0DeZ6cEYF6Iki7QjzHg2GzOIpD2pzIcKdiZ0MS2OC+tmCpVaXcGzufBto1dy8d1IV1cZzkcKtnjt/gD0+1iJvk2Uu7gQVk9LIlLzg2a3Uhktiu1cInKw9V5K+nXmHJfmSiNTzNZfZwO9mOEY/T/27mu7VR0IA/AvCZkO7r33XnBJmfd/r+MTpzhOOTHGmJzw3ey1NrlwHAQaSTPT2KYPs85Oqh33BrguXiQf3AHDleTGaat+72nPD70UxzG1li6U65uJQgETJYZrY/XDm+b1a1da+AQfFw/9DvVajnkpHNH+f/1Ub01Nkh/FJq5FjrJzhq94o0ZrqpkdQcGqm7i+tktE+koF0BVEJNo4ddxF7LHLwd5f2iIWLFkjX+5xU/M6BSrtIQRsWE8QWRMP0NK0N8BH5lLQk+SUfbjWQCxYqTT5kue4qZZCAXJnCAPj84kgUspZhtLnRZq485ofV5c4xeKAIFhcnfjfNbopaVBwCjuGMMiR6eRpL9nEtCqIiibem43zCXpmTfGZeCkoQM2lQT41cFNagoIiag2GUPCZh7ZOe52c3e4oJEZ4Rysq9GbJ8YlMXB4oMHwryK+Fg1uaKhQQfdlEWLgJrVIjIj2/hrNU6JG/xQcjk90LOlLlOOZIAKq98hALiFkk3/SFhhuqUECMtYkQaS3uJWjPynBz4lbN1wuFwvIuScfcnQaYzvOPOBMbQKPiRay45e8lGyVB/lkqfIlWe23rXiJM5rLFynQYAao5zGhHRVLFaUaeu2Uo5Rfdkek5uceFBqC7qZQQC8SsqNMFqgy3I6sUCGPFESpW6r68vdxHleHVUNCp6rLCtIKhGNXKotPLp7A3fyhmEUMEWrS7DdyO5lIQEluOkNkpmbLoiTJp4eDTc36JhqYik16NF6XsajI+/CzL50cAi9tl3L47kShkhjZuxNaDSYKXCF9We8n9Ue6+O5NS5ABKeSZVDqlJPJlXB1PAnGQYS8WbApdgK0EXstJtzzNxA2aFgpA3cQNrkw0T9CTD8KpOJ4oMYA89BpnLPjg46JYbANTlnZPpl5oOMHcQ82NepIulV+XyoskQuqZFAUg2cQvekLMlPelUbLy41+kdY8wAO2k503GnUG7joLUbYK/y0LFIL6+BwXqKmA8zutxjlUjUt92GRLjMOl1OWeEm5KKFVPLwEfQdXs572iWdjoixCWBA1MsryVKvwsypBqA15ADn9/3kYSGCNTdDmHM7jgnO5dHlCoKIhG4UhghZQ9CllLyG28g0HCzooCc7efncyDBBR1wbe1kiIdyV7K34dlLKMgwqABpDs5U1SIgCY7O7obqpdU5Xc+Uo3jD+nmMkFArKBlxFmLS+oMuIxRw3wsyKnRX0xMh2qg0wyQDVoCNV/GtJe30VDcky3XEFkCkA7QbA7jr1chWYtu42OlFiO2+peKNOtthjuTiN+At8u0lQUEqD0jLj5VLDoYpwNOt0mbyDm+EPq9eC2Ol6pW7nNlOAl0kk6EWHHdZGE65+yLRn0GwGmM15y9MY0Mx0yskhh1dVaM+olR9XLYlnfNTAHtuWunEm8ef4WKGAiHxREclyvu+mJx7C0UyTX7cvMeVtWC5JB2L9OJ3XPACZ/GRMSlERtHeHvR5RfpimtMQT8z7L7sr1cm0yBdo9ncgdTmv0Qhjl8cyRODarLNazuLPMJ1gjwPOUOr0Q5YyGUOyMoGqghI8NGMuk6WBjStmSAGROS9U3jdImTYqxxt5DUfRlkZQcnthuUp0o/c7E7UhoYzIUStcFHVHc/Pj9EOCtbqUSh8gf8LFB16F3OMLAhjr5JiKwdphVPjbl4CYH59l0d1jCnuYYSq6VfE3YHFY0p6FqvHmnAc6itXU/26Cs53BMpsbx6YkPbIOuRSkxhIGPyS9RjsAuqpY5zF4ePl6ZS27jXymhOKyovLQy4RrAsCcZwGymzSeFJJ0S1R3Dm1at+4jYCa1IV5N4HGoc12cWyKd+JNYIea58mO1/bSosiU1ps8EXzKm3LCToPWFNj5q/lpLtLCAjMOSjRObpehSrlp/h+rKC/JkhGlrWfwwARzEkbDNd5viUBsCezqp0oqsB4HZzntEmiZLUgHkXsSNsTdckRFrF1c0F+aJE5fwM2yYeezvp8K9rFkw4gO0On3LqU+zxDx2mrPosB7WTdJPl5MbBXtxk8sTKoJ+JcNJwTpAvIjKthtSMaT/WipOVjU/xuYm9r2aUnhjiyZhOCKswYrNFzartTIY91unFe2LHzEGNrmqLq+M18sfNRqXXEEfKEKQbPSk5q+zmGlRPxU9tDRtPHJdOiQ24nXsrrTdPrrX4JXCENRN0TUUVV9dQyJ9kdJ6GrbxFRHq93u8lLLf+UEtW8AMce/d3eLakE243dZqL9qCWGvGOwBu2oGsSXVwdz5M/i+jcCNJZWnWXhBDK055i/6GNL6hNDQetSQl7pvayJ1KkE1YTJ0ybzx66ubjJzKupRdfUxfW1FfKlgghxWrlMbbypHn6XZDU/+NhEnoFBrvsODtqJEo54lqBX7qbj6iQKXZxqlCc7+3EVncF/Yyyj0PUoc1wfL5Ivd4gWmVPV1K5uJAwroSjlx4o6Y/Z4cc/lMNOy0dx2xhOPN1bZl8zIiY0jY2HUe4XnU94t1WlmE0RGCydG43pea1bGXI3ODPCmZI2up8xxfSxPfkSz4zTPtdotb9Tt55erhtusW1bRzJTdZNW865QyM2+RThdzeCJNHGtl2zm7maQ93WMA+ISI6lwyHNPMzmIEe8YakXoD3lCWrsZoIAStBPkyQWQxnps6qp0xH+4bbbXd2G220k6ZTitv1LYDjs9wzgCWTer6S6/XXJlIWTzgxDynAjmpLqKyE3JjqkXXsuUIgSfIlwKijAGMM1Njsles7P8F0Cwnk9mcxvAN2ZoNVwP2dlxcKbQl3mHYu6/fZVaI/WtJV2KYCIOa+I3pAD/Gsp3EDP/qCuUHwStjkuGAtV1Bijvj+CBVmTQjcRgqAtoKXceGIQysR/4UBvgNZKq/w55T66wkzsJa2fVjOXmHj1THqYyishV4W7xA15AYmwiHY5E/PfwKLKcB0BZjW+IHnAw7CiY0M3U/wGf4YBK/A56M6QpEV0VYiuTPAr8Ib5s4ZjebZmUn8UHXcPCO5PgUT2mI7V3lPERaIjS1//cb4BnDMVZy++bgoZfiH2O6bjy1OQu/p8AlRwiNTAd2Gkj7NTukLLvK2vZCSffuPUe25wzPtDTV4gFwHjNJQetyhIVlBPkj5jgxXv6aEaDtytVygkhJuMVOYYwXKZ2suJXwmbI6BUqvawhN1iKfdBsn8sqvaUI6Swoh6EAx2ngxEiR+VXATBUEnR65zCI1ZJr/S7OOC2Aa/g5qnN/0sx4sHIkr/mvdYVIwpSMJDeLYK+VXFO411RyE34onjjHscexOF3lTaw9frZaI4CLjxgSAlhZ+4eZObIsOxtqEQKRHfH7Y3tRIHVPfdknN3gRd5IirLOAo4j9qnD35HySleJN+S8pMt8UhvD7NSXhf6UtXsBL0pmraHF52nDqzpBs4gc/jrUkX6TPRLjkwVomCC4HZB0F6kK6ilCgoR5TO1iU5vXAfsXQxACrlT/Jy68njrb0+bWKtAwVkjNPdBdXmdpWlPN5aIrtHhvi/XhS7ojSiO8KrjoyIHy69T/YgHP9fGvBoFpoSwyAL5Ju4YDqTMpg6/vjuM8hvg+bmvKPSecLPO6en2Fc7Q3jnFv34uguXWBUGnop5s5Sjk20TDQaOft54bJiecyN4HLFcp6fQFo9YGTPWoUNgSZ2CDWdHBZ+zs30md1KarIgVBd/Afbp4MQKQsTTzLCkHPkpG9/3erpmEp9BVRHabaDtClZ0kHZ1BTj7OP/5nDoN+J7DdyBXL6qNDligxh0Qzyxc1nTDxTO/RqGdVQsG0Vv++GIKyCx8Gr9Ex0OM7A5x5OtXJymv1jwbE5psv1ERoz4a+dTdPheOHpvyBReCj0mqDv1e7tgUIvDBXnyKZwyln0G/IvvQD+ZU4SdCGxQmg0P59WlJyvVlIjuwY0TZBC/0Ekiu67Na5zNGyc8nQx0f5c6rzZVX5HLrD/fbC8imOyGv0BoOXFlcv/SsY/bg+U2lz9W1Ogw8riZSyO8KzoXKI6xXt5ehXVSgkDQWfb4Cw8y3FCzlO/KEUiMI+/aQCoPTrTOsW+rIvhRrR2slOm8xmTJs7ARipOjTz8wbzJCl2mw+BDSGch9LyKU6+Ro8ggiphXU8gHxc3gDHMHp1QJ+eemQOjSRZQ2fAirKkrHwQcphQ4KEqdGEre122x7riB/jO4U38tm8IKbErGLz0bra45QzfULK0HIx69DYLZRcRusBdhy8OjqCYV8M3r43ijZYFDbGvb435vuf2pFFxBbFeFiVTpD0saplE7PMji2K3EgxXAbspFZFvtphS6TNPEt2U+uoHnr+OEfUHqYnkPYdgr9nMjixHz8xTFuL2/jdtjWUEjQpcQdvudsrCyXMy9OoXm1pguIHMImexcVA6okvmhnwFMctyK1oNr4uxq+Zy6SFSZb0Yz/byJDFyirCJ1TpZ+zzC9zKieRmQPPu3c6BUPsmil8y87mx1KaiAUwBUpPcQPZC+ZA0n397NHZBFhZFgUlXaxp+BafduMQ4E3OIl/cYnGyaeMWWnSGyld15eqIjJ2gwAghUvgP2t9b7P/akHwpN1NTU2W4hRX9nNI43QDV6aAYnbugJShIQ8Su++Vbnd4ct1PxdVTvNKfmAZHxSIFK/7FD/ZeZCzpTcjlwbIbbGV2yKsKT9KQQnRCAVSlYyS5iP7WjM+VbNsNNmQb9kFIbMZxYi8gVS69QwMrRGd2R16XzRKDbIOvTjyQ6mRzHKTWvE1nuDNFhJyhYSide5/mpOzpLPwrPlhL9RHnmcHwiNewv21507hA2bzwICpYSpfEdabz+C3st3tMPGHOOzzEnUttAvDJpWhSw6t8pb3IZnqZzFCKxvpClH1Cimu71QWU0UyhgiSi8qX+F6i8cAKZF34jgTtf3nJlLQdPjARDwisqBEYmMOdb5XzXFY3aBgibiIOBn2oLOoUdjanlHX4p+xvuZO2F/oifm7azpLG40TlDa6UhVbLxY26KgFaLxl4o6WfidzaYHLv2XXiTClZ/hHdqLg4B/2ruzrcSBIAzAvWUhARO2sAgRCJsom8II87//ew3OqAPIkqhHO3R/N9544YlJd1VSXfX9bjmS8GQ5SERvTJwW5kmKPOKryN/3VyoljgTqbWn2VTbCSVG6xgC1BL7YNEUb4A8qCyQwluiilk2c4KRq/T/b9DQ5npoJyD+KrjjiygghU1pJI5wwkuhRjYMVdAT0E1wfsc2iSJoA6Fz/TF+mRzWWJbakq2d9miVYeJx5U4o6oDhVTKKYsg2AEKuKLyB/32vZNBCTMwjuJLur8iYO89J3/595H1fphX6DIz6uj0XGEyKmgNZlS6toDQd591KFap8dfiP8ymPOtq7cYbWZRUyePNXeclsipjkpSZdWHZ4Z6ckVqX3qAXCcTH2ce+liS12rXYyAiyoE/GkrxONZRLplldU43qtOUhj/bFgZvFOf5FsWI/9R1ub4y8EplQHRYglTHFLajYP90NN5/x/4ItNYuoTQ/V/z8axS5DgudKVbrWQVIpaZlBd0P28Ujv8kXZwW07CKfSXj4Gm2BgA+H+A4cU+0eCYmjvPxqipHEfSxs+Si4Yfrgumty1dSPqhx3HG88NfTWrcYRF2bHDQ0gRULcVyY0k3wB1zjODHoFUw882Upgdtj3fYBL7i7dXOWUX7IpfnNR6fJ8Sx7lzdsgzH76LQ6tkDAWKOBo4okAVpM66b5FaY4of7LvW1UGtWoJemKQlmuv8jZL2OtJP0jY6LWkwM4vcH5IV2Dac5Yrgf8i44DF6/l3N8T+voICNxcUcu1LJlbqVppXvV3sclTrRXnWjOblEduC8dcU5KElW2krnCEfFMVekC070NZ3BGN9jz6NYz6OIg/kGRWop7a1OlzXA+nBZezvl4U43Y0ZFY5c2wkSDJGT5KDrt9uxHGaaCp6ZWTHDEYIneKQFSUJ2QvLUDATtgwP5zh6C5BYCQfwDkmsvb5+VG6po4sggzPENdHklc/iPT4gidGmMPObn0zi1x1fr+zjnJreAGRGuwIvBJ6ZXiEYM5LcGqh0H1bNB6KSuowjILUEWHttOhxA8zc2Mjc512LkA2YAhCNEujoJfFaEEzLVWUmqHsraAdRo3w9Dc3nVWZlOYU7JB/XxV1+tRGCMg8Rj0Fz8alm2WlcjpShluQeDMNbKf7gE9HpqYqPQuCVKaXEcNLItW610KOVojv6rBGHkQ2oZPPtlqbXkGREO4iuipQod2eSvTol8hD0MBOCoFvF2uO4ocyEmjJFnuRlJ6GXnsApARBQzxT5eNQFk9QOQNtTo11xitNsPJCk7evzXYCOj3CmaEPv4uL3o9dPYU0R1dCUK15FpdklSE+4Uh3QBfq3cv72YKWBPjRq2YpnQhXBNCA4ElCRidGpAxmx1pkv1vnmyzo3ArieipRO74wB41SWJWBUBgD9QJd950xbHG6nPPmrn2DMOoOJSksh9FgBXrxDuRRt7HLVqQS7JVR1waiSZXAMbBfWin2MHwgqydT/U4qID02kbJAk69jiApmLff1/RPAmwq5pTdS+8ALTdpiSJVmhig8vV9P77lLz7Cnb5uvozxSglCRTDLAeQXQyHaj4A5Spuqtg1UzYYVM7E4dgQA4Opef8Tu2B2BbY5oR6sqYqSj79UbQexwbzCCNtq9zoAUsXYxD8y9jz+JiM+bWLLVIc/ynAr+EfV/HfjysPU1A2F1VS+Cz0AoqfWCcjd+18IgW2hsu3x1ENtK1+MvEeFy37t0WDOsY0XAl0JpIzczLBdlYNemrN72MMryhXEKmus+ggBo1kR2NcjmiLaXaI2O4t3ZjoLUIaqX7/e5DLY480VTok01Vgmdpll1dcETSXMxy5f8aRIUwutYEdGt4HWVELr2MLXE5XfCWvqYd2dOojt6eoXMHFR087ZnS91Q/YU+wNWG+nyUO1SlSvY8kh2WKsFH3WFmBNNu0y0hi2Fq73pkU79vgpTN0nRLlZ55uONt/sA0EnbsjLmUGcC2sXKN3y8MTvvzlfbwa3+NKZdLtoNOV5lRozsoZZe/7VLxqwK3oimjvc11QyDLF7x7JOOeDS1MGvQzwq8EI2RymfkNBVRu9xtYoNzHi6WXb0JaKph7jIqZKulsWUbuiBIU5Bhu1dl5Y8IaZqmaZp21B/IMkx2LY2ONwAAAABJRU5ErkJggg==",
            "title" => "Explore the world of our projects",
            "desc" => "",
            "url" => get_field('map_page', 'option'),
            "last" => 0,
        );

        $projectsArr[] = $firstBlock;
    }

    $posts = get_posts($args);

    foreach ($posts as $post) {

        setup_postdata($post);

        $projectArr = array();
        $projectArr['type'] = get_field('column_6', $post->ID);

        if (get_field('column_6', $post->ID) == 'PV') {
            $projectArr['img'] = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgd2lkdGg9IjMyMnB4IiBoZWlnaHQ9IjE3N3B4IiB2aWV3Qm94PSIwIDAgMzIyIDE3NyIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj4NCiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDUyLjYgKDY3NDkxKSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4NCiAgICA8dGl0bGU+aWNvbnMvY2FzZXMvcHY8L3RpdGxlPg0KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPg0KICAgIDxnIGlkPSJpY29ucy9jYXNlcy9wdiIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+DQogICAgICAgIDxwYXRoIGQ9Ik01Ny45MzgsMTAyLjYyNCBMNDEuNzMyLDEwMi42MjQgTDQxLjczMiwxNDAgTDMyLjY4LDE0MCBMMzIuNjgsNDEuNTk2IEw1Ni4zMzIsNDEuNTk2IEM2Ny44MTczOTA4LDQxLjU5NiA3Ni44NDQ5NjcyLDQ0LjQ5MTYzNzcgODMuNDE1LDUwLjI4MyBDODkuOTg1MDMyOCw1Ni4wNzQzNjIzIDkzLjI3LDYzLjY5MDYxOTUgOTMuMjcsNzMuMTMyIEM5My4yNyw4MS4zMDgwNDA5IDkwLjA4MjM2NTIsODguMjY3MzA0NiA4My43MDcsOTQuMDEgQzc3LjMzMTYzNDgsOTkuNzUyNjk1NCA2OC43NDIwNTQsMTAyLjYyNCA1Ny45MzgsMTAyLjYyNCBaIE01Ny4zNTQsNDkuNjI2IEw0MS43MzIsNDkuNjI2IEw0MS43MzIsOTQuNTk0IEw1OC45Niw5NC41OTQgQzY2LjY0OTM3MTgsOTQuNTk0IDcyLjc1Njk3NzQsOTIuNTUwMDIwNCA3Ny4yODMsODguNDYyIEM4MS44MDkwMjI2LDg0LjM3Mzk3OTYgODQuMDcyLDc5LjI2NDAzMDcgODQuMDcyLDczLjEzMiBDODQuMDcyLDY1LjkyOTI5NzMgODEuNzM2MDIzNCw2MC4yMTEwMjEyIDc3LjA2NCw1NS45NzcgQzcyLjM5MTk3NjYsNTEuNzQyOTc4OCA2NS44MjIwNDIzLDQ5LjYyNiA1Ny4zNTQsNDkuNjI2IFogTTE1MS41MjQsMTQwIEwxMzkuMTE0LDE0MCBMMTA1LjY4LDQxLjU5NiBMMTE1LjQ2Miw0MS41OTYgTDEyOC40NTYsODAuNTc4IEwxNDUuMzkyLDEzMS45NyBMMTYyLjkxMiw4MC41NzggTDE3Ni4zNDQsNDEuNTk2IEwxODYuMTI2LDQxLjU5NiBMMTUxLjUyNCwxNDAgWiIgaWQ9IlBWIiBmaWxsPSIjMDAwMDAwIj48L3BhdGg+DQogICAgPC9nPg0KPC9zdmc+';
        } else if (get_field('column_6', $post->ID) == 'Wind') {
            $projectArr['img'] = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMC4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzMjIgMTc3IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzMjIgMTc3OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8dGl0bGU+aWNvbnMvY2FzZXMvdzwvdGl0bGU+DQo8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4NCjxnIGlkPSJpY29uc194MkZfY2FzZXNfeDJGX3ciPg0KCTxwYXRoIGlkPSJXIiBkPSJNNjQuOCwxNDBINTIuNEwyNi44LDQxLjZoOS41bDkuMiwzNi41bDEzLjMsNTIuN2M2LjUtMjMuMywxMS41LTQwLjgsMTUtNTIuN2wxMC4yLTM1aDEwLjdsMTAuOSwzNS4ybDE2LjQsNTMuMQ0KCQljNS40LTI0LjksOS4yLTQyLjYsMTEuNS01M2w4LjItMzYuOGg5LjFMMTI4LjUsMTQwSDExNmwtOS44LTMxLjFsLTE2LjYtNTRjLTMuNywxMy40LTguOSwzMS40LTE1LjYsNTRMNjQuOCwxNDB6Ii8+DQo8L2c+DQo8L3N2Zz4NCg==';
        }

        $projectArr['title'] = get_field('column_13', $post->ID);
        $projectArr['desc'] = get_field('column_14', $post->ID);
        $projectArr['url'] = get_the_permalink($post->ID);
        $projectArr['last'] = $lastBlock;
        $projectsArr[] = $projectArr;
    }
    wp_reset_postdata();

    //echo "callback: ".$is_callback;

    if (!empty($is_callback)) {
        return $projectsArr;
    } else {
        echo json_encode($projectsArr);
        die();
        wp_die();
    }
}

add_action('wp_ajax_expertjson', 'expertjson_callback');
add_action('wp_ajax_nopriv_expertjson', 'expertjson_callback');

function expertjson_callback() {

    $expertArr = array();
    $expertID = $_POST['expert_id'];
    $quoteID = $_POST['quote_id'];

    if (!empty($expertID)) {
        $expertPhotos = get_field('images', $expertID);
        $expertPhotoID = $expertPhotos[rand(0, (count($expertPhotos) - 1))]['ID'];
        $expertPhotoSrc = wp_get_attachment_image_src($expertPhotoID, 'full');

        //expert personal data
        $expertArr['name'] = get_field('name', $expertID);
        $expertArr['image'] = $expertPhotoSrc[0];
        $expertArr['hobby'] = get_field('hobby', $expertID);
        $expertArr['education'] = get_field('education', $expertID);
        $expertArr['working_evergy_since'] = get_field('working_evergy_since', $expertID);
        $expertArr['working_in_the_sector_since'] = get_field('working_in_the_sector_since', $expertID);
        $expertArr['language'] = get_field('language', $expertID);

        //expert quote
        if ($quoteID != "") {
            $expertArr['quote'] = get_field('quote', $quoteID);
            $expertArr['quote_translated'] = get_field('translation', $quoteID);
        } else {
            $args = array(
                'meta_query' => array(
                    array(
                        'key' => 'expert',
                        'value' => $expertID,
                    ),
                ),
                'post_type' => 'quotes_and_news',
                'posts_per_page' => 1,
                'orderby' => 'rand',
            );

            $quotes = get_posts($args);

            foreach ($quotes as $quote) {
                setup_postdata($quote);
                $expertArr['quote'] = get_field('quote', $quote->ID);
                $expertArr['quote_translated'] = get_field('translation', $quote->ID);
            }
            wp_reset_postdata();
        }
    }

    echo json_encode($expertArr);

    wp_reset_postdata();
    die();
    wp_die();
}

// map json file generation ajax callback

add_action('wp_ajax_mapjson', 'mapjson_callback');
add_action('wp_ajax_nopriv_mapjson', 'mapjson_callback');

function mapjson_callback() {

    $status = array('status' => 'error');

    $args = array(
        'meta_query' => array(
            array(
                'key' => 'column_2',
                'value' => '',
                'compare' => '!=',
            ),
            array(
                'key' => 'column_3',
                'value' => '',
                'compare' => '!=',
            ),
        ),
        'post_type' => 'projects',
        'post_status' => 'publish',
        'numberposts' => -1,
    );

    $projectsPosts = get_posts($args);

    $projects[] = array(
        "1" => "Project ID",
        "2" => "Latitude",
        "3" => "Longitude",
        "4" => "Projectname",
        "5" => "Country",
        "6" => "Type of Project",
        "7" => "Year of Mandate",
        "8" => "Evergy Scope of Work",
        "9" => "Installed Capacity in MW",
        "10" => "Homepage Case",
        "11" => "Client",
        "12" => "Year of Commissioning",
        "13" => "Descriptive Headline",
        "14" => "Copytext",
        "15" => "Key Technical Data",
        "16" => "Headlie 1",
        "17" => "Bullets 1",
        "18" => "Headlie 2",
        "19" => "Bullets 2",
        "20" => "Type of Installation",
        "21" => "PV Module Manufacturer",
        "22" => "PV Inverter Manufacturer",
        "23" => "W WT Manufacturer",
        "24" => "WT Type",
        "25" => "N� of Turbines",
        "26" => "PDF link",
        "27" => "Details link",
    );

    foreach ($projectsPosts as $projectData) {
        setup_postdata($projectData);

        $projectURL = "";
        $projectPDF = "";

        if (get_field('column_10', $projectData->ID) == "Y") {
            $projectURL = get_the_permalink($projectData->ID);
            $projectPDF = get_the_permalink($projectData->ID) . "/?pdf=" . $projectData->ID;
        }

        $field15 = '';

        $theList = preg_split('/\r\n|\r|\n/', get_field('column_15', $projectData->ID));

        foreach ($theList as $theItem) {
            if ($theItem != "")
                $field15 .= $theItem . "<br>";
        }


        $projects[] = array(
            "1" => get_field('column_1', $projectData->ID),
            "2" => get_field('column_2', $projectData->ID),
            "3" => get_field('column_3', $projectData->ID),
            "4" => get_field('column_4', $projectData->ID),
            "5" => get_field('column_5', $projectData->ID),
            "6" => get_field('column_6', $projectData->ID),
            "7" => get_field('column_7', $projectData->ID),
            "8" => get_field('column_8', $projectData->ID),
            "9" => get_field('column_9', $projectData->ID),
            "10" => get_field('column_10', $projectData->ID),
            "11" => get_field('column_11', $projectData->ID),
            "12" => get_field('column_12', $projectData->ID),
            "13" => get_field('column_13', $projectData->ID),
            "14" => get_field('column_14', $projectData->ID),
            "15" => $field15,
            "16" => get_field('column_16', $projectData->ID),
            "17" => get_field('column_17', $projectData->ID),
            "18" => get_field('column_18', $projectData->ID),
            "19" => get_field('column_19', $projectData->ID),
            "20" => get_field('column_20', $projectData->ID),
            "21" => get_field('column_21', $projectData->ID),
            "22" => get_field('column_22', $projectData->ID),
            "23" => get_field('column_23', $projectData->ID),
            "24" => get_field('column_24', $projectData->ID),
            "25" => get_field('column_25', $projectData->ID),
            "26" => $projectPDF,
            "27" => $projectURL,
        );
    }
    wp_reset_postdata();

    $fp = fopen(get_template_directory() . '/assets/csvjson.json', 'w');
    if ($fp) {
        if (fwrite($fp, json_encode($projects))) {
            $status = json_encode(array('status' => 'success'));
        }
        fclose($fp);
    }
    echo $status;
    die();
    wp_die();
}

// Add ACF RTE type

function evergy_mce_toolbars($toolbars) {
    $toolbars['Jobs'] = array();
    $toolbars['Jobs'][1] = array('formatselect', 'numlist', 'bullist', 'italic', 'bold', 'fullscreen');
    $toolbars['Team'] = array();
    $toolbars['Team'][1] = array('link', 'italic', 'bold', 'fullscreen', 'bullist');
    return $toolbars;
}

add_filter('acf/fields/wysiwyg/toolbars', 'evergy_mce_toolbars');


add_filter('quicktags_settings', 'cyb_quicktags_settings');

function cyb_quicktags_settings($qtInit) {
    //Set to emtpy string, empty array or false won't work. It must be set to ","
    $qtInit['buttons'] = ',';
    return $qtInit;
}

function evergy_change_mce_block_formats($init) {
    $block_formats = array(
        'Paragraph=p',
        'Heading 6=h6',
    );
    $init['block_formats'] = implode(';', $block_formats);

    return $init;
}

add_filter('tiny_mce_before_init', 'evergy_change_mce_block_formats');

function evergy_404() {
    if (get_post_type() == 'projects') {
        if (get_field('column_10') != 'Y') {
            wp_redirect(get_field('404_page', 'options'));
            exit();
        }
    } else if (get_post_type() == 'quotes_and_news') {
        wp_redirect(get_field('404_page', 'options'));
        exit();
    }
    if (is_404()) {
        wp_redirect(get_field('404_page', 'options'));
        exit();
    }
}

add_action('template_redirect', 'evergy_404');

function evergy_get_open_grap_data() {
    if (get_post_type(get_the_ID()) == 'jobs') {
        $header_logo = wp_get_attachment_image_src(get_field('shares_logo', 'option'), 'full');
        echo '<meta property="og:type" content="website">'
        . '<meta property="og:site_name" content="' . get_bloginfo('name') . '">'
        . '<meta property="og:url" content="' . get_the_permalink() . '">'
        . '<meta property="og:image" content="' . $header_logo[0] . '">'
        . '<meta property="og:title" content="' . get_field('social_sharing_title') . '">'
        . '<meta name="twitter:card" content="summary">'
        . '<meta name="twitter:image" content="' . $header_logo[0] . '">'
        . '<meta name="twitter:title" content="' . get_field('social_sharing_title') . '">';
    }
}

?>