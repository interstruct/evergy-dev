<?php get_header(); ?>
<?php get_template_part('template-parts/common/header'); ?>

<?php while (have_posts()) : the_post(); ?>
    <main class="main js-main">
        <?php
        if (have_rows('page_builder')) {
            while (have_rows('page_builder')) {
                the_row();

                $styling = '';
                $marginTop = get_sub_field('margin-top');
                $marginBottom = get_sub_field('margin-bottom');
                $paddingTop = get_sub_field('padding-top');
                $paddingBottom = get_sub_field('padding-bottom');


                If ($marginTop != 0 || $marginBottom != 0 || $paddingTop != 0 || $paddingBottom != 0) {
                    $styling = 'style="';
                    if ($marginTop != 0) {
                        $styling .= 'margin-top:' . $marginTop . 'px; ';
                    }
                    if ($marginBottom != 0) {
                        $styling .= 'margin-bottom:' . $marginBottom . 'px; ';
                    }
                    if ($paddingTop != 0) {
                        $styling .= 'padding-top:' . $paddingTop . 'px; ';
                    }
                    if ($paddingBottom != 0) {
                        $styling .= 'padding-bottom:' . $paddingBottom . 'px; ';
                    }
                    $styling .= '"';
                }
                ?>
                <div <?php evergy_the_component_wrapper(); ?> <?php echo $styling; ?>>
                    <?php get_template_part('template-parts/builder/' . get_row_layout() . '/' . get_row_layout()); ?>
                </div>
                <?php
            }
        }
        ?>
    </main>
<?php endwhile; // end of the loop.    ?>
<?php get_template_part('template-parts/common/footer'); ?>
<?php get_footer(); ?>