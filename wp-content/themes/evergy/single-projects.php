<?php get_header(); ?>
<?php get_template_part('template-parts/common/header'); ?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    if (!empty(get_field('map_image'))) {
        $header_map_land = wp_get_attachment_image_src(get_field('map_image'), 'full');
    } else {
        $header_map_land = wp_get_attachment_image_src(get_field('case_map_fallback_image', 'option'), 'full');
    }
    if (!empty(get_field('map_image_port'))) {
        $header_map_port = wp_get_attachment_image_src(get_field('map_image_port'), 'full');
    } else {
        $header_map_port = wp_get_attachment_image_src(get_field('case_map_fallback_image_portrait', 'option'), 'full');
    }
    ?>

    <main class="main js-main">
        <div class="case">
            <div class="case__wrapper ">
                <!-- begin case__hero -->
                <a href="<?php echo get_the_ID() . "/?pdf=" . get_the_ID(); ?>" class="case__download" download>
                    <span>download<br />case</span>
                    <i>
                        <svg class="icon icon-icon-download-general">
                        <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-icon-download-general"></use>
                        </svg>
                    </i>
                </a>
                <div class="case__hero">
                    <div class="case__hero-bg case__hero-bg--landscape" style="background-image: url('<?php echo $header_map_land[0]; ?>');"></div>
                    <div class="case__hero-bg case__hero-bg--portrait" style="background-image: url('<?php echo $header_map_port[0]; ?>');"></div>
                    <div class="wrapper">
                        <div class="row">
                            <div class="col-dl-17 col-dl-push-1 col-tl-push-1 col-tl-15">
                                <h1><?php the_field('column_13'); ?></h1>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end case__hero -->
                <!-- begin case__content -->
                <div class="case__content">
                    <div class="wrapper">
                        <div class="row">
                            <div class="case__content-wrapper col-dl-10 col-dl-push-4 col-dm-11 col-tl-15  col-ts-push-4 col-ts-15 col-ml-push-1 col-ml-15">
                                <!-- begin case__inner -->
                                <div class="case__inner">
                                    <h2 class="case__title"><?php the_field('column_14'); ?></h2>
                                    <div class="text">
                                        <p>
                                            <strong><?php the_field('column_16'); ?></strong>
                                        </p>
                                        <ul>
                                            <?php
                                            $theList = preg_split('/\r\n|\r|\n/', get_field('column_17'));

                                            foreach ($theList as $theItem) {
                                                if ($theItem != "")
                                                    echo "<li>" . $theItem . "</li>";
                                            }
                                            ?>
                                        </ul>
                                        <p>
                                            <strong><?php the_field('column_18'); ?></strong>
                                        </p>
                                        <ul>
                                            <?php
                                            $theList = preg_split('/\r\n|\r|\n/', get_field('column_19'));

                                            foreach ($theList as $theItem) {
                                                if ($theItem != "")
                                                    echo "<li>" . $theItem . "</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end case__inner -->
                            </div>
                            <div class="case__sidebar-wrapper col-dl-5 col-dl-push-2 col-dm-push-1 col-dm-5 col-tl-push-4 col-tl-15 col-ts-push-4 col-ts-15 col-ml-push-1 col-ml-15">
                                <div class="case__sidebar">
                                    <h2 class="case__title case__title_mob"><?php the_field('column_14'); ?></h2>
                                    <div class="case__block ">
                                        <p class="case__block-title">
                                            <strong>Country</strong>
                                        </p>
                                        <p class="case__block-text"><?php the_field('column_5'); ?></p>
                                    </div>
                                    <?php /*
                                      <div class="case__block ">
                                      <p class="case__block-title">
                                      <strong>Client</strong>
                                      </p>
                                      <p class="case__block-text"><?php the_field('column_11'); ?></p>
                                      </div>
                                     */ ?>
                                    <div class="case__block ">
                                        <p class="case__block-title">
                                            <strong>Year of Mandate</strong>
                                        </p>
                                        <p class="case__block-text"><?php the_field('column_7'); ?></p>
                                    </div>
                                    <div class="case__block ">
                                        <p class="case__block-title">
                                            <strong>Year of Commisioning</strong>
                                        </p>
                                        <p class="case__block-text"><?php the_field('column_12'); ?></p>
                                    </div>
                                    <div class="case__block ">
                                        <p class="case__block-title">
                                            <strong>Key Technical Data</strong>
                                        </p>
                                        <div class="case__block-text">
                                            <ul>
                                                <?php
                                                $theList = preg_split('/\r\n|\r|\n/', get_field('column_15'));
                                                
                                                foreach ($theList as $theItem) {
                                                    if ($theItem != "")
                                                        echo "<li>" . $theItem . "</li>";
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end case__content -->
            </div>
        </div>
        <!-- begin quote-slider -->
        <div class="quote-slider-automatic">
            <div class="quote-slider__in swiper-container js-quote-slider">
                <div class="swiper-wrapper">
                    <?php
                    $args = array(
                        'post_type' => 'quotes_and_news',
                        'orderby' => 'rand',
                        'meta_query' => array(
                            array(
                                'key' => 'case',
                                'value' => get_the_ID(),
                                'compare' => '=',
                            )
                        ),
                        'fields' => 'ids',
                        'posts_per_page' => 1,
                    );
                    $slidesIDs = new WP_Query($args);

                    wp_reset_query();

                    $slidesArr = array();
                    $popupsArr = array();
                    $sliderCount = 0;
                    foreach ($slidesIDs->posts AS $slideID) {
                        $sliderCount++;
                        $slideArr = array(
                            "slide-image" => "",
                            "slide-image-caption-1" => "",
                            "slide-image-caption-2" => "",
                            "slide-image-caption-2-1" => "",
                            "slide-text-1" => "",
                            "slide-text-2" => "",
                            "slide-link-1" => "",
                            "slide-link-2" => "",
                        );

                        $popupArr = array(
                            "number" => 1,
                            "name" => "",
                            "image" => "",
                            "hobby" => "",
                            "education" => "",
                            "working_evergy_since" => "",
                            "working_in_the_sector_since" => "",
                            "language" => "",
                            "quote" => "",
                            "quote_translated" => "",
                        );

                        if (get_field('type', $slideID) == 'quote') {
                            $expertPhotos = get_field('images', get_field('expert', $slideID));

                            // push data to popup arr
                            $popupArr['number'] = $sliderCount;
                            $popupArr['name'] = get_field('name', get_field('expert', $slideID));
                            $popupArr['image'] = $expertPhotos[rand(0, (count($expertPhotos) - 1))]['ID'];
                            $popupArr['hobby'] = get_field('hobby', get_field('expert', $slideID));
                            $popupArr['education'] = get_field('education', get_field('expert', $slideID));
                            $popupArr['working_evergy_since'] = get_field('working_evergy_since', get_field('expert', $slideID));
                            $popupArr['working_in_the_sector_since'] = get_field('working_in_the_sector_since', get_field('expert', $slideID));
                            $popupArr['language'] = get_field('language', get_field('expert', $slideID));
                            $popupArr['quote'] = get_field('quote', $slideID);
                            $popupArr['quote_translated'] = get_field('translation', $slideID);


                            // push data to slide arr
                            $slideArr['slide-image'] = $expertPhotos[rand(0, (count($expertPhotos) - 1))]['ID'];
                            $slideArr['slide-image-caption-1'] = get_field('name', get_field('expert', $slideID));
                            $slideArr['slide-image-caption-2'] = get_field('hobby', get_field('expert', $slideID));
                            $slideArr['slide-image-caption-2-1'] = get_field('working_evergy_since', get_field('expert', $slideID));
                            $slideArr['slide-text-1'] = get_field('quote', $slideID);
                            $slideArr['slide-text-2'] = get_field('translation', $slideID);

                            if (!empty(get_field('expert', $slideID))) {
                                if (empty(get_field('expert_card_button_title', $slideID))) {
                                    $tempTitle = explode(" ", trim(get_field('name', get_field('expert', $slideID))));
                                    $button1Title = "meet " . $tempTitle[0];
                                } else {
                                    $button1Title = get_field('expert_card_button_title', $slideID);
                                }
                                $slideArr['slide-link-1'] = '<a href="#" data-quote-id="' . $slideID . '" data-modal-id="' . get_field('expert', $slideID) . '" class="button button_primary"><span>' . $button1Title . '</span></a>';
                            }

                            if (!empty(get_field('case', $slideID))) {
                                $slideArr['slide-link-2'] = '<a href="' . get_permalink(get_field('case', $slideID)) . '" target="_blank" class="button button_primary"><span>' . get_field('case_details_button_title', $slideID) . '</span></a>';
                            }
                        } else if (get_field('type', $slideID) == 'news') {
                            $slideArr['slide-image'] = get_field('image', $slideID);
                            $slideArr['slide-image-caption-1'] = get_field('caption_below_image_bold', $slideID);
                            $slideArr['slide-image-caption-2'] = get_field('caption_below_image_regular', $slideID);
                            $slideArr['slide-text-1'] = get_field('headline', $slideID);
                            $slideArr['slide-text-2'] = get_field('text_below_the_headline', $slideID);

                            if (get_field('link1_type', $slideID) == "file" && !empty(get_field('file1', $slideID))) {
                                $slideArr['slide-link-1'] = '<a href="' . get_field('file1', $slideID) . '" targe="_blank" class="button button_primary"><span>' . get_field('button_title1', $slideID) . '</span></a>';
                            } else if (get_field('link1_type', $slideID) == "url") {
                                $link1Url = get_field('link1', $slideID);
                                if (!empty($link1Url)) {
                                    $slideArr['slide-link-1'] = '<a href="' . $link1Url['url'] . '" target="' . $link1Url['target'] . '" class="button button_primary"><span>' . $link1Url['title'] . '</span></a>';
                                }
                            }

                            if (get_field('link2_type', $slideID) == "file" && !empty(get_field('file2', $slideID))) {
                                $slideArr['slide-link-2'] = '<a href="' . get_field('file2', $slideID) . '" targe="_blank" class="button button_primary"><span>' . get_field('button_title2', $slideID) . '</span></a>';
                            } else if (get_field('link2_type', $slideID) == "url") {
                                $link2Url = get_field('link2', $slideID);
                                if (!empty($link2Url)) {
                                    $slideArr['slide-link-2'] = '<a href="' . $link2Url['url'] . '" target="' . $link2Url['target'] . '" class="button button_primary"><span>' . $link2Url['title'] . '</span></a>';
                                }
                            }
                        }

                        $slidesArr[] = $slideArr;
                        $popupsArr[] = $popupArr;
                    }
                    ?>

                    <?php foreach ($slidesArr AS $slide) { ?>

                        <div class="quote-slide swiper-slide">
                            <div class="wrapper">
                                <div class="row">
                                    <div class="col-dl-16 col-tl-push-1 col-ml-15">
                                        <div class="quote-slide__content">
                                            <figure class="quote-slide__pic">
                                                <div class="quote-slide__img">
                                                    <?php
                                                    //echo qbai_get_image($slide['slide-image'], 'slider_adapt', false); 
                                                    $sliderImage = wp_get_attachment_image_src($slide['slide-image'], 'full');
                                                    ?>
                                                    <img src="<?php echo $sliderImage[0]; ?>">

                                                </div>
                                                <?php if (!empty($slide['slide-image-caption-1']) && !empty($slide['slide-image-caption-1'])) { ?>
                                                    <figcaption>
                                                        <?php if (!empty($slide['slide-image-caption-1'])) { ?>
                                                            <span class="quote-slide__name"><?php echo $slide['slide-image-caption-1']; ?></span>
                                                        <?php } ?>
                                                        <?php if (!empty($slide['slide-image-caption-2'])) { ?>
                                                            <span class="quote-slide__desc"><?php
                                                                echo trim($slide['slide-image-caption-2']);
                                                                if (!empty($slide['slide-image-caption-2-1'])) {
                                                                    echo ", " . $slide['slide-image-caption-2-1'];
                                                                }
                                                                ?></span>
                                                        <?php } ?>
                                                    </figcaption>
                                                <?php } ?>
                                            </figure>
                                            <div class="quote-slide__in">
                                                <blockquote class="quote-slide__quote"><?php echo $slide['slide-text-1']; ?></blockquote>
                                                <div class="quote-slide__msg"><?php echo $slide['slide-text-2']; ?></div>
                                                <?php echo $slide['slide-link-1']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                </div>
                <button class="quote-slider__arrow quote-slider__arrow_prev js-quote-prev">
                    <svg class="icon icon-prev">
                    <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-prev"></use>
                    </svg>
                </button>
                <button class="quote-slider__arrow quote-slider__arrow_next js-quote-next">
                    <svg class="icon icon-next">
                    <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-next"></use>
                    </svg>
                </button>
                <div class="swiper-pagination"></div>
            </div> 
        </div>
        <!-- end quote-slider -->
    </main>
<?php endwhile; // end of the loop.   ?>
<?php get_template_part('template-parts/common/footer'); ?>
<?php get_footer(); ?>