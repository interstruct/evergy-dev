<?php /* Template Name: Map */ ?>
<?php get_header(); ?>
      <main class="main js-main">
        <div class="map js-map">
          <div id="map" class="map__in" data-json="<?php echo get_stylesheet_directory_uri(); ?>/assets/csvjson.json"></div>
          <button class="map__close" onclick="history.back(-1)">
            <svg class="icon icon-close-big">
              <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-close-big"></use>
            </svg>
          </button>
        </div>
      </main>
    </div>
<?php get_footer(); ?>