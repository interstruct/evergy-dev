<?php
$header_logo = wp_get_attachment_image_src(get_field('header_logo', 'option'), 'full');
?>
<!-- test commit 2 -->
<!-- visual grid -->
<div class="grid">
    <div class="wrapper">
        <div class="row">
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
            <div class="col-dl-1"></div>
        </div>
    </div>
</div>
<!-- begin header -->
<div class="headers js-headers">
<header class="header">
    <div class="wrapper">
        <div class="row">
            <!-- begin header__logo -->
            <div class="header__logo col-dl-6 col-dl-push-4 col-tm-13">
                <div class="header__logo-in">
                    <div class="logo logo_header">
                        <a href="<?php echo icl_get_home_url() ?>" class="logo__in">
                            <img src="<?php echo $header_logo[0]; ?>" alt="" />
                        </a>
                    </div>
                </div>
            </div>
            <!-- end header__logo -->

            <?php
            
            if (( $locations = get_nav_menu_locations() ) && isset($locations['primary'])) {
                $menu = get_term($locations['primary']);

                $menu_items = wp_get_nav_menu_items($menu->term_id);
            }


            if (count($menu_items) > 0) {
                ?>
                <!-- begin header__nav -->
                <div class="header__nav col-dl-10 col-dl-push-1">
                    <nav class="nav">
    <?php
    foreach ((array) $menu_items as $key => $menu_item) { //is-active
        $active_class = '';
        if ($menu_item->url == get_permalink()) {
            $active_class = 'is-active';
        }
        ?>
                            <a href="<?php echo $menu_item->url; ?>" class="nav__link <?php echo $active_class; ?>" ><?php echo $menu_item->title; ?></a>
                        <?php } ?>
                    </nav>
                </div>
                <!-- end header__nav -->
                <!-- begin header__burger -->
                <div class="header__burger col-dl-10 col-dl-push-8 col-tm-3 col-tm-push-1">
                    <button class="burger js-burger">
                        <span class="burger__in">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </button>
                </div>
                <!-- end header__burger -->
<?php } ?>
        </div>
    </div>
</header>
<!-- end header -->
<!-- begin header -->
<header class="header-sticky">
    <div class="wrapper">
        <div class="row">
            <!-- begin header-sticky__text -->
            <div class="header-sticky__text col-dl-6 col-dl-push-1 col-tm-13">
                <div class="header-sticky__text-in">
                    <p><?php the_field('header_alt_text', 'option'); ?></p>
                </div>
            </div>
            <!-- end header-sticky__text -->
<?php if (count($menu_items) > 0) { ?>
                <!-- begin header__nav -->
                <div class="header-sticky__nav col-dl-10 col-dl-push-1">
                    <nav class="nav">
    <?php
    foreach ((array) $menu_items as $key => $menu_item) { //is-active
        $active_class = '';
        if ($menu_item->url == get_permalink()) {
            $active_class = 'is-active';
        }
        ?>
                            <a href="<?php echo $menu_item->url; ?>" class="nav__link <?php echo $active_class; ?>" ><?php echo $menu_item->title; ?></a>
                        <?php } ?>
                    </nav>
                </div>
                <!-- end header__nav -->
                <!-- begin header__burger -->
                <div class="header-sticky__burger col-dl-10 col-dl-push-8 col-tm-3 col-tm-push-1">
                    <button class="burger burger_sticky js-burger">
                        <span class="burger__in">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </button>
                </div>
                <!-- end header__burger -->
<?php } ?>
        </div>
    </div>
</header>
</div>
<!-- end header -->