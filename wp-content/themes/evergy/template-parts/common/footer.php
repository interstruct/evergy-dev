<?php
$footer_logo = wp_get_attachment_image_src(get_field('footer_logo', 'option'), 'full');
?>
<!-- begin footer -->
<footer class="footer">
    <!-- begin footer__logo -->
    <div class="footer__logo wrapper">
        <div class="row">
            <div class="col-dl-16 col-dl-push-4">
                <div class="logo logo_footer">
                    <a href="/" class="logo__in">
                        <img src="<?php echo $footer_logo[0]; ?>" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end footer__logo -->
    <!-- begin footer__contacts -->
    <div class="footer__contacts wrapper">
        <div class="row">
            <div class="footer__col col-dl-6 col-dl-push-4 col-dm-7 col-ds-8 col-tl-7 col-tm-9 col-ml-16">
                <div class="footer__text">
                    <?php the_field('footer_info_column_1', 'option'); ?>
                </div>
            </div>
            <div class="footer__col col-dl-5 col-dl-push-1 col-ds-8 col-tl-9 col-tm-7 col-ml-16 col-ml-push-4">
                <div class="footer__text">
                    <?php the_field('footer_info_column_2', 'option'); ?>
                </div>
            </div>
            <div class="footer__col footer__col_special col-dl-5 col-dl-push-1 col-dm-4 col-ds-16 col-ds-push-4">
                <div class="footer__text">
                    <?php the_field('footer_info_column_3', 'option'); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- end footer__contacts -->
    <!-- begin footer__copyright -->
    <div class="footer__copyright wrapper">
        <div class="row">
            <div class="col-dl-16 col-dl-push-4 col-ds-10 col-tm-16">
                <div class="footer__copy">
                    <p><?php the_field('footer_copyright_text', 'option'); ?></p>
                </div>
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer',
                    'menu' => '',
                    'container' => '',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'footer__meta',
                    'menu_id' => '',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'items_wrap' => '<ul class="%2$s">%3$s</ul>',
                    'depth' => 0,
                    'walker' => '',
                ));
                ?>
            </div>
        </div>
    </div>
    <!-- end footer__copyright -->
</footer>
<!-- end footer -->