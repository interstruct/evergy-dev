<!-- begin job -->
<div class="wrapper">
    <div class="row">
        <div class="col-dl-12 col-dl-push-4 col-ds-16 col-ml-15 col-ml-push-1">

            <?php
            $shareButtonsBlockTitle = get_sub_field('share_buttons_block_title');
            $mailto = get_sub_field('mailto_email');

            if (have_rows('jobs')) {
                while (have_rows('jobs')) {
                    the_row();
                    ?>

                    <div class="job js-job <?php
                    if (get_sub_field('open_accordion')) {
                        echo 'is-active';
                    }
                    ?>" data-other-page-scroll-id="<?php echo sanitize_title(get_field('headline', get_sub_field('job'))); ?>">
                        <div class="job__title">
                            <svg class="state-icon state-icon_lg js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 32" shape-rendering="geometricPrecision">
                                <g fill="#EC6B06">
                                    <path d="M2,32h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,31.1,0.9,32,2,32z" />
                                    <path d="M2,25h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,24.1,0.9,25,2,25z" />
                                    <path d="M2,18h6c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2c-1.1,0-2,0.9-2,2l0,0C0,17.1,0.9,18,2,18z" />
                                    <path d="M0,2L0,2c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,0,0,0.9,0,2z" />
                                    <path d="M0,9L0,9c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2l0,0c0-1.1-0.9-2-2-2H2C0.9,7,0,7.9,0,9z" />
                                </g>
                            </svg>
                            <svg class="state-icon state-icon_md js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 31" shape-rendering="geometricPrecision">
                                <g fill="#EC6B06">
                                    <path d="M1.5,31h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,28,0,28.7,0,29.5l0,0
                                          C0,30.3,0.7,31,1.5,31z" />
                                    <path d="M1.5,24h19c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-19C0.7,21,0,21.7,0,22.5l0,0
                                          C0,23.3,0.7,24,1.5,24z" />
                                    <path d="M1.5,17h7c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-7C0.7,14,0,14.7,0,15.5l0,0
                                          C0,16.3,0.7,17,1.5,17z" />
                                    <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h19C21.3,3,22,2.3,22,1.5l0,0C22,0.7,21.3,0,20.5,0h-19
                                          C0.7,0,0,0.7,0,1.5z" />
                                    <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h19c0.8,0,1.5-0.7,1.5-1.5l0,0C22,7.7,21.3,7,20.5,7h-19
                                          C0.7,7,0,7.7,0,8.5z" />
                                </g>
                            </svg>
                            <svg class="state-icon state-icon_sm js-state-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 31" shape-rendering="geometricPrecision">
                                <g fill="#EC6B06">
                                    <path d="M1.5,31h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,28,0,28.7,0,29.5l0,0
                                          C0,30.3,0.7,31,1.5,31z" />
                                    <path d="M1.5,24h9c0.8,0,1.5-0.7,1.5-1.5l0,0c0-0.8-0.7-1.5-1.5-1.5h-9C0.7,21,0,21.7,0,22.5l0,0
                                          C0,23.3,0.7,24,1.5,24z" />
                                    <path d="M1.5,17H4c0.8,0,1.5-0.7,1.5-1.5l0,0C5.5,14.7,4.8,14,4,14H1.5C0.7,14,0,14.7,0,15.5l0,0
                                          C0,16.3,0.7,17,1.5,17z" />
                                    <path d="M0,1.5L0,1.5C0,2.3,0.7,3,1.5,3h9C11.3,3,12,2.3,12,1.5l0,0C12,0.7,11.3,0,10.5,0h-9
                                          C0.7,0,0,0.7,0,1.5z" />
                                    <path d="M0,8.5L0,8.5C0,9.3,0.7,10,1.5,10h9c0.8,0,1.5-0.7,1.5-1.5l0,0C12,7.7,11.3,7,10.5,7h-9
                                          C0.7,7,0,7.7,0,8.5z" />
                                </g>
                            </svg>
                            <h2 class="js-job-title"><?php the_field('headline', get_sub_field('job')); ?></h2>
                        </div>
                        <div class="job__in">
                            <div class="job__content js-job-content">
                                <div class="job__content-in">
                                    <div class="job__date">
                                        <p><?php the_field('sub_headline', get_sub_field('job')); ?></p>
                                    </div>
                                    <div class="job__inner">
                                        <div class="job__texts">
                                            <div class="job__text">
                                                <?php the_field('description', get_sub_field('job')); ?>
                                            </div>
                                            <div class="job__button">
                                                <a href="mailto:<?php echo $mailto; ?>" class="button button_primary"><span><?php the_sub_field('button_text'); ?></span></a>
                                            </div>
                                        </div>
                                        <div class="job__socials">
                                            <div class="socialbox">
                                                <div class="socialbox__text"><?php echo $shareButtonsBlockTitle; ?></div>
                                                <ul class="social-links">
                                                    <li>
                                                        <a class="js-twitter-share" href="<?php echo get_the_permalink(get_sub_field('job')); ?>">
                                                            <svg class="icon icon-twitter">
                                                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-twitter"></use>
                                                            </svg>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="js-linkedin-share" href="<?php echo get_the_permalink(get_sub_field('job')); ?>">
                                                            <svg class="icon icon-linkedin">
                                                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-linkedin"></use>
                                                            </svg>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="js-xing-share" href="<?php echo get_the_permalink(get_sub_field('job')); ?>">
                                                            <svg class="icon icon-xing">
                                                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-xing"></use>
                                                            </svg>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="mailto:?subject=<?php the_field('social_sharing_title', get_sub_field('job')); ?>&body=<?php echo get_the_permalink(get_sub_field('job')); ?>">
                                                            <svg class="icon icon-mail">
                                                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-mail"></use>
                                                            </svg>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>         

        </div>
    </div>
</div>
<!-- end job -->