<div class="wrapper">
    <div class="row">
        <div class="col-dl-12 col-dl-push-4 col-tm-15 col-ml-push-1">
            <div class="text__in">
                <?php the_sub_field('text'); ?>
            </div>
        </div>
    </div>
</div>