<div class="wrapper">
    <div class="row">
        <div class="col-dl-12 col-dl-push-4 col-ds-push-4 col-tl-15 col-tl-push-4 col-ml-push-1">
            <div class="team__content">
                <h3><?php the_sub_field('headline'); ?></h3>
                <?php if (the_sub_field('text') != "") { ?>
                    <?php the_sub_field('text'); ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-dl-16 col-dl-push-4 col-ds-15 col-ds-push-4 col-tl-push-4 col-ml-push-1">
            <div class="team__in">
                <?php
                if (have_rows('infographic_blocks')) {
                    while (have_rows('infographic_blocks')) {
                        the_row();
                        $Image = wp_get_attachment_image_src(get_sub_field('image'), 'full');
                        ?>
                        <div class="team__item">
                            <div class="team-box">
                                <div class="team-box__icon">
                                    <img src="<?php echo $Image[0]; ?>" alt="" />
                                </div>
                                <div class="team-box__content">
                                    <h6 class="team-box__title"><?php the_sub_field('headline'); ?></h6>
                                    <div class="team-box__text"><?php the_sub_field('text'); ?></div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>