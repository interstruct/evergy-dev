<div class="wrapper">
    <?php if (get_sub_field('show_graphic')) { ?>
        <div class="row">
            <div class="col-dl-16 col-dl-push-4 col-ml-15 col-ml-push-1">
                <div class="services__graphiс">
                    <svg class="icon icon-wind1">
                    <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-wind1"></use>
                    </svg>
                    <svg class="icon icon-wind2">
                    <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-wind2"></use>
                    </svg>
                    <svg class="icon icon-windkraft-2x">
                    <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-windkraft-2x"></use>
                    </svg>
                    <svg class="icon icon-windkraft-3x">
                    <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-windkraft-3x"></use>
                    </svg>
                    <svg class="icon icon-pv-2x">
                    <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-pv-2x"></use>
                    </svg>
                    <svg class="icon icon-pv-3x">
                    <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-pv-3x"></use>
                    </svg>
                    <svg class="icon icon-sun">
                    <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-sun"></use>
                    </svg>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-dl-9 col-dl-push-4 col-tl-10 col-tm-12 col-ml-10 col-mm-14 col-ml-push-1">
            <div class="services__title">
                <h3><?php the_sub_field('headline'); ?></h3>
            </div>
        </div>
    </div>
    <?php if (have_rows('services')) { ?>
        <div class="row">
            <div class="col-dl-16 col-dl-push-4 col-ml-15 col-ml-push-1">
                <div class="services__list">
                    <?php
                    while (have_rows('services')) {
                        the_row();
                        $serviceImg = wp_get_attachment_image_src(get_sub_field('image'), 'full');
                        ?>
                        <div class="services__item">
                            <div class="service">
                                <div class="service__icon">
                                    <img src="<?php echo $serviceImg[0]; ?>" alt="<?php the_sub_field('title'); ?>" />
                                </div>
                                <div class="service__in">
                                    <div class="service__title">
                                        <h6><?php the_sub_field('title'); ?></h6>
                                    </div>
                                    <div class="service__text">
                                        <?php the_sub_field('features'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>