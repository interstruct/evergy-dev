<div class="projects-pattern js-projects" data-json="<?php echo get_stylesheet_directory_uri(); ?>/assets/projects.json" data-wp-request="true">
    <div class="wrapper">
        <div class="row">
            <div class="col-dl-12 col-dl-push-4 col-ds-push-4 col-tl-15 col-tl-push-4 col-ml-push-1">
                <div class="projects-pattern__content">
                    <h3><?php the_sub_field('headline'); ?></h3>
                    <p><?php the_sub_field('copy_text'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="projects-pattern__filter col-dl-4 col-dl-push-1 col-ml-16 col-ml-push-0">
                <div class="filter-box js-filter-box">
                    <div class="filter-box__col">
                        <div class="filter-box__name">filter<br />cases<br />by</div>
                        <div class="filter-box__list">
                            <a href="#" class="filter-box__case js-filter-btn" data-filter-type="PV">
                                <svg class="icon icon-filter-pv">
                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-filter-pv"></use>
                                </svg>
                            </a>
                            <a href="#" class="filter-box__case js-filter-btn" data-filter-type="Wind">
                                <svg class="icon icon-filter-w">
                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-filter-w"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="filter-box__col">
                        <div class="filter-box__name">download<br />track<br />record</div>
                        <div class="filter-box__list">
                            <a href="<?php echo get_option('pv-pdf-url'); ?>" class="filter-box__load" download>
                                <svg class="icon icon-download-pv">
                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-download-pv"></use>
                                </svg>
                            </a>
                            <a href="<?php echo get_option('wind-pdf-url'); ?>" class="filter-box__load" download>
                                <svg class="icon icon-download-w">
                                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-download-w"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-dl-12 col-ml-15 col-ml-push-1">
                <div class="projects-pattern__list-wrapper js-projects-list-wrapper">
                    <div class="projects-pattern__list js-projects-list">
                        <?php
                        $projects_first_package = projectjson_callback("yes", 1);
                        $load_more_btn = "";
                        foreach ($projects_first_package as $item) {
                            if ($load_more_btn=="" && $item['last']!=0){
                                $load_more_btn = 'hidden';
                            }
                            if ($item['type'] != 'spec') {
                                ?>
                                <div class="projects-pattern__item" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                    <a href="<?php echo $item['url']; ?>" class="project">
                                        <span class="project__in">
                                            <span class="project__img">
                                                <img src="<?php echo $item['img']; ?>" alt="">
                                            </span>
                                            <span class="project__text"><?php echo $item['desc']; ?></span>
                                            <span class="project__title"><?php echo $item['title']; ?></span>
                                            <div class="project__link-wrapper">
                                                <span class="project__link">Read more</span>
                                            </div>
                                        </span>
                                    </a>
                                </div>
                            <?php } else { ?>
                                <div class="projects-pattern__item" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                    <a href="<?php echo get_field('map_page', 'option'); ?>" class="project project_special">
                                        <span class="project__in">
                                            <span class="project__img">
                                                <img src="<?php echo $item['img']; ?>" alt="">
                                            </span>
                                            <span class="project__title project__title_special"><?php echo $item['title']; ?></span>
                                        </span>
                                    </a>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-dl-16 col-dl-push-4 col-ml-15 col-ml-push-1">
                <div class="projects-pattern__more-wrapper">
                    <button class="projects-pattern__more-btn <?php echo $load_more_btn; ?> js-more-btn">
                        <svg class="icon icon-load-more">
                        <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-load-more"></use>
                        </svg>
                        <strong>load more</strong>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end projects -->

