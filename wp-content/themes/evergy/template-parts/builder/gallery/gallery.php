<div class="gallery__in js-gallery is-loading">
    <div class="gallery__navigation">
        <div class="gallery__navigation-in">
            <button class="gallery__prev js-gallery-prev is-hidden">
                <svg class="icon icon-arrow-left">
                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-left"></use>
                </svg>
            </button>
            <button class="gallery__next js-gallery-next">
                <svg class="icon icon-arrow-right">
                <use xlink:href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-right"></use>
                </svg>
            </button>
        </div>
    </div>
    <div class="gallery__preloader">
        <div class="preloader">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="slideshow">

        <?php
        $common_caption = get_sub_field('common_caption');

        if (have_rows('slides')) {
            while (have_rows('slides')) {
                the_row();
                $slide_img = wp_get_attachment_image_src(get_sub_field('image'), 'full');
                ?>
                <div class="slide">
                    <div class="slide__wrap">
                        <div class="slide__img" style="background-image: url('<?php echo $slide_img[0]; ?>')"></div>
                        <div class="slide__in">
                            <div class="slide__wrapper wrapper">
                                <div class="row">                                      
                                    <div class="col-dl-16 col-dl-push-14 col-dm-push-12 col-tl-push-4">
                                        <div class="slide__subtitle">
                                            <p class="slide__message"><?php echo $common_caption; ?></p>
                                            <p class="slide__headline"><?php the_sub_field('slide_title'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>