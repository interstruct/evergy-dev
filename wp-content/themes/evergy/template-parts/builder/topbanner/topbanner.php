<?php $bannerImage = wp_get_attachment_image_src(get_sub_field('image'), 'full'); ?>

<div class="wrapper">
    <div class="row">
        <div class="col-dl-7 col-ds-8 col-tl-13 col-tl-push-1 col-tm-15">
            <div class="topbanner__title">
                <h1><?php the_sub_field('headline'); ?></h1>
            </div>
            <?php if (!empty(get_sub_field('navigation_title'))) { ?>
                <div class="topbanner__sticky">
                    <div class="sticky-nav sticky-nav--real js-sticky-nav" data-top="162">
                        <h3 class="sticky-nav__title"><?php the_sub_field('navigation_title'); ?></h3>
                        <nav class="sticky-nav__list js-sticky-nav-list"></nav>  
                    </div>
                    <div class="sticky-nav sticky-nav--pseudo js-sticky-nav-pseudo">
                        <h3 class="sticky-nav__title"><?php the_sub_field('navigation_title'); ?></h3>
                        <nav class="sticky-nav__list js-sticky-nav-list"></nav>  
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-dl-9 col-ds-8 col-tl-16 col-tl-push-3 col-tm-push-2 col-ml-push-1 col-mm-push-0">
            <div class="topbanner__img">
                <img src="<?php echo $bannerImage[0]; ?>" alt="" />
            </div>
        </div>
    </div>
</div>
<?php if (!empty(get_sub_field('navigation_title'))) { ?>
    <div class="topbanner__mob">
        <div class="wrapper">
            <div class="row">
                <div class="col-tl-5 col-tl-push-1">
                    <div class="sticky-nav js-sticky ">
                        <h3 class="sticky-nav__title"><?php the_sub_field('navigation_title'); ?></h3>
                        <nav class="sticky-nav__list js-sticky-nav-list">
                        </nav>  
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php
if (get_sub_field('job_teaser')) {
    $jobObj = get_sub_field('teaser_link');
    ?>
    <!-- begin job-teaser -->
    <a href="<?php echo $jobObj['url']; ?>" class="job-teaser js-scroll-other-page-link">
        <span class="job-teaser__bg js-job-teaser-bg"></span>
        <span class="job-teaser__in">
            <span class="job-teaser__content">
                <span class="job-teaser__text"><?php the_sub_field('title_line_1'); ?></span>
                <h6 class="job-teaser__title"><?php echo $jobObj['title']; ?></h6>
            </span>
            <span class="job-teaser__img">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/job-teaser/1.png" alt="" />
            </span>
        </span>
    </a>
    <!-- end button-teaser -->
<?php } ?>

