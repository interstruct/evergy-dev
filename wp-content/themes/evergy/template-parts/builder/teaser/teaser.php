<div class="wrapper">
    <div class="row">
        <div class="col-dl-13 col-dl-push-3 col-ml-16 col-ml-push-0">
            <?php
            $mainLinkTarget = get_sub_field('main_link_target');
            if (get_sub_field('main_link_type') == 'int'){
                $mainLink = get_sub_field('main_link_int');
            } else if (get_sub_field('main_link_type') == 'ext'){
                $mainLink = get_sub_field('main_link_ext');
            }
            ?>
            <a href="<?php echo $mainLink; ?>" data-target = "<?php echo $mainLinkTarget; ?>" class="teaser__box spec-link js-teaser-spec-link">
                <div class="teaser__title">
                    <h2><?php the_sub_field('headline'); ?></h2>
                </div>
                <div class="teaser__in">
                    <div class="teaser__text"><?php the_sub_field('text'); ?></div>
                    <?php if (have_rows('links')) { ?>
                        <nav class="teaser__nav">
                            <?php
                            while (have_rows('links')) {
                                the_row();
                                $linkArr = get_sub_field('link');
                                ?>
                                <span data-href="<?php echo $linkArr['url']; ?>"  class="teaser__link js-scroll-other-page-link"><?php echo $linkArr['title']; ?></span>
                            <?php }
                            ?>
                        </nav>  
                    <?php } ?>
                </div>
            </a>
        </div>
    </div>
</div>